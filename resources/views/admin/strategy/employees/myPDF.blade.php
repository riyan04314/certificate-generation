<!DOCTYPE html>
<html>
<head>
<title>{{$certificate['certificate_title']}}</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<style>
body{
	font-family: DejaVu Sans, sans-serif;
	background: url('images/certificate_bgimage.jpg') no-repeat 0 0;
    background-image-resize: 6;	
	margin-header:0mm;
}

<?php /*
@page {
	margin-header: 5mm;
}

background: url('images/certificate_bgimage.jpg') no-repeat 0 0;
@page { sheet-size: A3-L; }
@page bigger { sheet-size: 3348mm 2317mm; }
@page toc { sheet-size: A4; }*/?>

</style>
</head>
<body  style="font-family:Arial">
<?php /*
<table width="100%" align="center" border="1">
	<tr>
		<th align="right" width="40%">{{$employee['institution_name']}}</th>
		<th align="center">
			<img src="{{ public_path() }}\images\ulogo.jpg" width="60" />
		</th>
		<th align="left" width="40%">Higher Colleges of Technology </th>
	</tr>
</table>
*/ ?>
<table width="100%" align="center" border="0">
	<tr>
		<th align="center">
			<img src="{{ public_path() }}\images\certificate_logo.png"  />
		</th>
	</tr>
</table>



<?php /*<div align="center" style="margin-top:0px;">
	<img src="{{ public_path() }}\images\certificate_logo.png"  />
</div>*/ ?>
<h2 align="center" style="margin-top:20px; color:#BF9140">{{$certificate['certificate_title']}}</h2>
<h3 align="center" style="margin-top:40px; color:#000">{{$certificate['certificate_subtitle']}}</h3>
<h2 align="center" style="margin-top:25px; color:#000;">110{{$employee['emp_category'].' '.$employee['first_name'].' '.$employee['last_name']}}</h2>
<h3 align="center" style="margin:15px 180px; padding:10px; line-height:45px; border-top:1px solid #BF9140; border-bottom:1px solid #BF9140;">{{strip_tags($certificate['certificate_details'])}}</h3>

@if(isset($employee['certificate_approve_by']) && $employee['certificate_approve_by']>0)
<table width="100%" align="center" style="margin-top:10px;">	
	<tr>
		<td>
			<h3>{{$user[$employee['certificate_approve_by']]['signature_name']}} / {{$user[$employee['certificate_approve_by']]['name']}}</h3>
			<h3 style="color:#CCC;">{{$user[$employee['certificate_approve_by']]['department']}}</h3>
		</td>
		<td></td>
		<td align="left">		
			<img src="{{ public_path() }}\images\qrcode\qrcode-{{$qrstr}}.png" width="120" />
		</td>
	</tr>
</table>
@endif
</body>
</html>

