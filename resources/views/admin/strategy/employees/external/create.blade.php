@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        
		@if(Auth::user()->roles[0]->title == 'TraineeOfficer')
			{{ trans('global.ctraining') }}
		@else		
			{{ trans('global.create') }} 
			@if($type == 1){{ trans('cruds.employee.internal_certificate') }}@endif 
			@if($type == 2){{ trans('cruds.employee.title_singular') }}@endif
		
		@endif
		
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.employees.store") }}" enctype="multipart/form-data">
            @csrf
			<div class="form-group @if($type == 2){{ 'd-none' }}@endif">
                <label for="emp_id">{{ trans('cruds.employee.fields.emp_id') }}</label>
                <input class="form-control {{ $errors->has('emp_id') ? 'is-invalid' : '' }}" type="text" name="emp_id" id="emp_id" value="{{ old('emp_id', '') }}">
                @if($errors->has('emp_id'))
                    <div class="invalid-feedback">
                        {{ $errors->first('emp_id') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.employee.fields.last_name_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required">{{ trans('cruds.employee.fields.emp_category') }}</label>
                <select class="form-control {{ $errors->has('emp_category') ? 'is-invalid' : '' }}" name="emp_category" id="emp_category" required>
                    <option value disabled {{ old('emp_category', null) === null ? 'selected' : '' }}>{{ trans('global.pleaseSelect') }}</option>
                    @foreach(App\Employee::EMP_CATEGORY_SELECT as $key => $label)
                        <option value="{{ $key }}" {{ old('emp_category', '1') === (string) $key ? 'selected' : '' }}>{{ $label }}</option>
                    @endforeach
                </select>
                @if($errors->has('emp_category'))
                    <div class="invalid-feedback">
                        {{ $errors->first('emp_category') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.employee.fields.emp_category_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="first_name">{{ trans('cruds.employee.fields.first_name') }}</label>
                <input type="hidden" name="created_by" value="{{ Auth::user()->id }}" >
                <input class="form-control {{ $errors->has('first_name') ? 'is-invalid' : '' }}" type="text" name="first_name" id="first_name" value="{{ old('first_name', '') }}" required>
                @if($errors->has('first_name'))
                    <div class="invalid-feedback">
                        {{ $errors->first('first_name') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.employee.fields.first_name_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="last_name">{{ trans('cruds.employee.fields.last_name') }}</label>
                <input class="form-control {{ $errors->has('last_name') ? 'is-invalid' : '' }}" type="text" name="last_name" id="last_name" value="{{ old('last_name', '') }}">
                @if($errors->has('last_name'))
                    <div class="invalid-feedback">
                        {{ $errors->first('last_name') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.employee.fields.last_name_helper') }}</span>
            </div>
			
			
			<div class="form-group">
                <label for="emailid">{{ trans('cruds.employee.fields.emailid') }}</label>
                <input class="form-control {{ $errors->has('emailid') ? 'is-invalid' : '' }}" type="email" name="emailid" id="emailid" value="{{ old('emailid', '') }}">
                @if($errors->has('emailid'))
                    <div class="invalid-feedback">
                        {{ $errors->first('emailid') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.employee.fields.last_name_helper') }}</span>
            </div>
			
			
			
			
			
			
			
			
			
			
			
            <div class="form-group">
                <label for="institution_name">{{ trans('cruds.employee.fields.institution_name') }}</label>
                <input class="form-control {{ $errors->has('institution_name') ? 'is-invalid' : '' }}" type="text" name="institution_name" id="institution_name" value="{{ old('institution_name', '') }}">
                @if($errors->has('institution_name'))
                    <div class="invalid-feedback">
                        {{ $errors->first('institution_name') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.employee.fields.institution_name_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="department">{{ trans('cruds.user.fields.department') }}</label>
                <input @if(!in_array(Auth::user()->roles[0]->title,array('Admin'))){{ 'readonly' }}@endif class="form-control {{ $errors->has('department') ? 'is-invalid' : '' }}" type="text" name="department" id="department" value="{{ old('department', Auth::user()->department) }}" required>
                @if($errors->has('department'))
                    <div class="invalid-feedback">
                        {{ $errors->first('department') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.name_helper') }}</span>
            </div>			
            <div class="form-group">
                <label for="default_certificate">{{ trans('cruds.employee.fields.default_certificate') }}</label>
                <?php /*<input class="form-control {{ $errors->has('default_certificate') ? 'is-invalid' : '' }}" type="number" name="default_certificate" id="default_certificate" value="{{ old('default_certificate', '1') }}" step="1">*/?>
                <select class="form-control {{ $errors->has('default_certificate') ? 'is-invalid' : '' }}"  name="default_certificate">
					@foreach($certificate as $val)
						<option value="{{$val['id']}}">{{$val['certificate_title']}}</option>						
					@endforeach
				</select>
                @if($errors->has('default_certificate'))
                    <div class="invalid-feedback">
                        {{ $errors->first('default_certificate') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.employee.fields.default_certificate_helper') }}</span>
            </div>	


            <div class="form-group">
                <label for="issue_date">{{ trans('cruds.employee.fields.issue_date') }}</label>
                <input class="form-control date {{ $errors->has('issue_date') ? 'is-invalid' : '' }}" type="text" name="issue_date" id="issue_date" value="{{ old('issue_date') }}">
                @if($errors->has('issue_date'))
                    <div class="invalid-feedback">
                        {{ $errors->first('issue_date') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.employee.fields.issue_date_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="serial_no">{{ trans('cruds.employee.fields.serial_no') }}</label>
                <input class="form-control {{ $errors->has('serial_no') ? 'is-invalid' : '' }}" type="text" name="serial_no" id="serial_no" value="{{ old('serial_no', '') }}">
                @if($errors->has('serial_no'))
                    <div class="invalid-feedback">
                        {{ $errors->first('serial_no') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.employee.fields.serial_no_helper') }}</span>
            </div>

			
            <div class="form-group">
                <label>{{ trans('cruds.employee.fields.certificate_approval_status') }}</label>
                <select disabled  class="form-control {{ $errors->has('certificate_approval_status') ? 'is-invalid' : '' }}" name="certificate_approval_status" id="certificate_approval_status">
                    <option value disabled {{ old('certificate_approval_status', null) === null ? 'selected' : '' }}>{{ trans('global.pleaseSelect') }}</option>
                    @foreach(App\Employee::CERTIFICATE_APPROVAL_STATUS_SELECT as $key => $label)	
						@if(Auth::user()->roles[0]->title == 'TraineeOfficer')
							<option value="{{ $key }}" {{ old('certificate_approval_status', '2') === (string) $key ? 'selected' : '' }}>{{ $label }}</option>
						@else
							<option value="{{ $key }}" {{ old('certificate_approval_status', '1') === (string) $key ? 'selected' : '' }}>{{ $label }}</option>
						@endif
                    @endforeach
                </select>
                @if($errors->has('certificate_approval_status'))
                    <div class="invalid-feedback">
                        {{ $errors->first('certificate_approval_status') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.employee.fields.certificate_approval_status_helper') }}</span>
            </div>
            <div class="form-group d-none">
                <label class="required" for="certificate_approve_by">{{ trans('cruds.employee.fields.certificate_approve_by') }}</label>
                <input class="form-control {{ $errors->has('certificate_approve_by') ? 'is-invalid' : '' }}" type="number" name="certificate_approve_by" id="certificate_approve_by" value="{{ old('certificate_approve_by', '0') }}" step="1" required>
                @if($errors->has('certificate_approve_by'))
                    <div class="invalid-feedback">
                        {{ $errors->first('certificate_approve_by') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.employee.fields.certificate_approve_by_helper') }}</span>
            </div>
            <div class="form-group d-none">
                <label for="certificate_approve_date">{{ trans('cruds.employee.fields.certificate_approve_date') }}</label>
                <input class="form-control date {{ $errors->has('certificate_approve_date') ? 'is-invalid' : '' }}" type="text" name="certificate_approve_date" id="certificate_approve_date" value="{{ old('certificate_approve_date') }}">
                @if($errors->has('certificate_approve_date'))
                    <div class="invalid-feedback">
                        {{ $errors->first('certificate_approve_date') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.employee.fields.certificate_approve_date_helper') }}</span>
            </div>
			<div class="form-group d-none">
                <label for="certificate_qrcode">{{ trans('cruds.employee.fields.certificate_qrcode') }}</label>
                <textarea class="form-control {{ $errors->has('certificate_qrcode') ? 'is-invalid' : '' }}" name="certificate_qrcode" id="certificate_qrcode">{{ old('certificate_qrcode') }}</textarea>
                @if($errors->has('certificate_qrcode'))
                    <div class="invalid-feedback">
                        {{ $errors->first('certificate_qrcode') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.employee.fields.certificate_qrcode_helper') }}</span>
            </div>
            
            <div class="form-group d-none">
                <label>{{ trans('cruds.employee.fields.employee_type') }}</label>
				
                <select class="form-control {{ $errors->has('employee_type') ? 'is-invalid' : '' }}" name="employee_type" id="employee_type">
                    <option value disabled {{ old('employee_type', null) === null ? 'selected' : '' }}>{{ trans('global.pleaseSelect') }}</option>
                    @foreach(App\Employee::EMPLOYEE_TYPE_SELECT as $key => $label)
                        <option value="{{ $key }}" {{ old('employee_type', '1') === (string) $key ? 'selected' : '' }}>{{ $label }}</option>
                    @endforeach
                </select>
				<input type="text"  name="employee_type" value="{{$type}}" />
                @if($errors->has('employee_type'))
                    <div class="invalid-feedback">
                        {{ $errors->first('employee_type') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.employee.fields.employee_type_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection