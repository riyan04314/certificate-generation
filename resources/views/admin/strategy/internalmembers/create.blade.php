@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.internalmember.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.internalmembers.store") }}" enctype="multipart/form-data">
            @csrf
			<div class="form-group">
				<label class="required" for="emp_id">{{ trans('cruds.internalmember.fields.emp_id') }}</label>
				<input class="form-control {{ $errors->has('emp_id') ? 'is-invalid' : '' }}" type="text" name="emp_id" id="emp_id" value="{{ old('emp_id', '') }}" required>
				@if($errors->has('emp_id'))
					<div class="invalid-feedback">
						{{ $errors->first('emp_id') }}
					</div>
				@endif
				<span class="help-block d-none">{{ trans('cruds.internalmember.fields.emp_id_helper') }}</span>
			</div>
            
			<div class="form-group">
                <label class="required">{{ trans('cruds.internalmember.fields.emp_category') }}</label>
                <select class="form-control {{ $errors->has('emp_category') ? 'is-invalid' : '' }}" name="emp_category" id="emp_category" required>
                    <option value disabled {{ old('emp_category', null) === null ? 'selected' : '' }}>{{ trans('global.pleaseSelect') }}</option>
                    @foreach(App\Internalmember::EMP_CATEGORY_SELECT as $key => $label)
                        <option value="{{ $key }}" {{ old('emp_category', '1') === (string) $key ? 'selected' : '' }}>{{ $label }}</option>
                    @endforeach
                </select>
                @if($errors->has('emp_category'))
                    <div class="invalid-feedback">
                        {{ $errors->first('emp_category') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.internalmember.fields.emp_category_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="first_name">{{ trans('cruds.internalmember.fields.first_name') }}</label>
                <input class="form-control {{ $errors->has('first_name') ? 'is-invalid' : '' }}" type="text" name="first_name" id="first_name" value="{{ old('first_name', '') }}" required>
                @if($errors->has('first_name'))
                    <div class="invalid-feedback">
                        {{ $errors->first('first_name') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.internalmember.fields.first_name_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="last_name">{{ trans('cruds.internalmember.fields.last_name') }}</label>
                <input class="form-control {{ $errors->has('last_name') ? 'is-invalid' : '' }}" type="text" name="last_name" id="last_name" value="{{ old('last_name', '') }}">
                @if($errors->has('last_name'))
                    <div class="invalid-feedback">
                        {{ $errors->first('last_name') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.internalmember.fields.last_name_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="institution_name">{{ trans('cruds.internalmember.fields.institution_name') }}</label>
                <input class="form-control {{ $errors->has('institution_name') ? 'is-invalid' : '' }}" type="text" name="institution_name" id="institution_name" value="{{ old('institution_name', '') }}">
                @if($errors->has('institution_name'))
                    <div class="invalid-feedback">
                        {{ $errors->first('institution_name') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.internalmember.fields.institution_name_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="emailid">{{ trans('cruds.internalmember.fields.emailid') }}</label>
                <input class="form-control {{ $errors->has('emailid') ? 'is-invalid' : '' }}" type="email" name="emailid" id="emailid" value="{{ old('emailid') }}">
                @if($errors->has('emailid'))
                    <div class="invalid-feedback">
                        {{ $errors->first('emailid') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.internalmember.fields.emailid_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="contact_no">{{ trans('cruds.internalmember.fields.contact_no') }}</label>
                <input class="form-control {{ $errors->has('contact_no') ? 'is-invalid' : '' }}" type="text" name="contact_no" id="contact_no" value="{{ old('contact_no', '') }}">
                @if($errors->has('contact_no'))
                    <div class="invalid-feedback">
                        {{ $errors->first('contact_no') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.internalmember.fields.contact_no_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="department">{{ trans('cruds.internalmember.fields.department') }}</label>
                
				<input  class="form-control {{ $errors->has('department') ? 'is-invalid' : '' }}" type="text" name="department" id="department" value="{{ old('department', Auth::user()->department) }}" required @if(!in_array(Auth::user()->roles[0]->title,array('Admin'))){{ 'readonly' }}@endif>				
                @if($errors->has('department'))
                    <div class="invalid-feedback">
                        {{ $errors->first('department') }}
                    </div>
                @endif
                <span class="help-block d-none">{{ trans('cruds.internalmember.fields.department_helper') }}</span>
            </div>
						
			
			<div class="d-none">
            <div class="form-group">
                <label>{{ trans('cruds.internalmember.fields.certificate_approval_status') }}</label>
                <select class="form-control {{ $errors->has('certificate_approval_status') ? 'is-invalid' : '' }}" name="certificate_approval_status" id="certificate_approval_status">
                    <option value disabled {{ old('certificate_approval_status', null) === null ? 'selected' : '' }}>{{ trans('global.pleaseSelect') }}</option>
                    @foreach(App\Internalmember::CERTIFICATE_APPROVAL_STATUS_SELECT as $key => $label)
                        <option value="{{ $key }}" {{ old('certificate_approval_status', '1') === (string) $key ? 'selected' : '' }}>{{ $label }}</option>
                    @endforeach
                </select>
                @if($errors->has('certificate_approval_status'))
                    <div class="invalid-feedback">
                        {{ $errors->first('certificate_approval_status') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.internalmember.fields.certificate_approval_status_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="certificate_approve_by">{{ trans('cruds.internalmember.fields.certificate_approve_by') }}</label>
                <input class="form-control {{ $errors->has('certificate_approve_by') ? 'is-invalid' : '' }}" type="number" name="certificate_approve_by" id="certificate_approve_by" value="{{ old('certificate_approve_by', '0') }}" step="1">
                @if($errors->has('certificate_approve_by'))
                    <div class="invalid-feedback">
                        {{ $errors->first('certificate_approve_by') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.internalmember.fields.certificate_approve_by_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="certificate_approve_date">{{ trans('cruds.internalmember.fields.certificate_approve_date') }}</label>
                <input class="form-control date {{ $errors->has('certificate_approve_date') ? 'is-invalid' : '' }}" type="text" name="certificate_approve_date" id="certificate_approve_date" value="{{ old('certificate_approve_date') }}">
                @if($errors->has('certificate_approve_date'))
                    <div class="invalid-feedback">
                        {{ $errors->first('certificate_approve_date') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.internalmember.fields.certificate_approve_date_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="certificate_qrcode">{{ trans('cruds.internalmember.fields.certificate_qrcode') }}</label>
                <textarea class="form-control {{ $errors->has('certificate_qrcode') ? 'is-invalid' : '' }}" name="certificate_qrcode" id="certificate_qrcode">{{ old('certificate_qrcode') }}</textarea>
                @if($errors->has('certificate_qrcode'))
                    <div class="invalid-feedback">
                        {{ $errors->first('certificate_qrcode') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.internalmember.fields.certificate_qrcode_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="default_certificate">{{ trans('cruds.internalmember.fields.default_certificate') }}</label>
                <input class="form-control {{ $errors->has('default_certificate') ? 'is-invalid' : '' }}" type="number" name="default_certificate" id="default_certificate" value="{{ old('default_certificate', '1') }}" step="1">
                @if($errors->has('default_certificate'))
                    <div class="invalid-feedback">
                        {{ $errors->first('default_certificate') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.internalmember.fields.default_certificate_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="created_by">{{ trans('cruds.internalmember.fields.created_by') }}</label>
                <input class="form-control {{ $errors->has('created_by') ? 'is-invalid' : '' }}" type="number" name="created_by" id="created_by" value="{{ old('created_by', '1') }}" step="1">
                @if($errors->has('created_by'))
                    <div class="invalid-feedback">
                        {{ $errors->first('created_by') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.internalmember.fields.created_by_helper') }}</span>
            </div>
			</div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection