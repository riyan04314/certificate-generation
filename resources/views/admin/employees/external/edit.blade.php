@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} 
		
		@if($employee->employee_type == 2)
			{{ trans('cruds.employee.title_singular') }}
		@else
			{{ trans('cruds.internalmember.title_singular') }}
		@endif		
		
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.employees.update", [$employee->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf   
			
			
			<div class="form-row">						
				<div class="form-group col-md-4">
					<label class="required" for="department">{{ trans('cruds.user.fields.department') }}</label>
					<?php /*
					<input @if(!in_array(Auth::user()->roles[0]->title,array('Admin'))){{ 'readonly' }}@endif class="form-control {{ $errors->has('department') ? 'is-invalid' : '' }}" type="text" name="department" id="department" value="{{ old('department', Auth::user()->department) }}" required>*/?>
					
					<select class="form-control {{ $errors->has('department') ? 'is-invalid' : '' }}"  name="department">					
						@foreach($department as $val)
							@if(Auth::user()->department == '')
								<option @if($employee->department == $val->department) selected @endif>
									{{$val->department}}
								</option>
							@elseif(Auth::user()->department == $val->department)
								<option>{{$val->department}}</option>
							@endif											
						@endforeach
					</select>                				
					
					@if($errors->has('department'))
						<div class="invalid-feedback">
							{{ $errors->first('department') }}
						</div>
					@endif
					<span class="help-block">{{ trans('cruds.user.fields.name_helper') }}</span>
				</div>			
				<div class="form-group col-md-4">
					<label for="default_certificate">{{ trans('cruds.employee.fields.default_certificate') }}</label>
					<input class="form-control d-none {{ $errors->has('default_certificate') ? 'is-invalid' : '' }}" type="number" name="default_certificate" id="default_certificate" value="{{ old('default_certificate', $employee->default_certificate) }}" step="1">
					<select class="form-control {{ $errors->has('default_certificate') ? 'is-invalid' : '' }}"  name="default_certificate">
						@foreach($certificate as $val)					
							<option value="{{$val['id']}}" @if($val['id'] == $employee->default_certificate){{ 'selected' }}@endif>{{$val['certificate_title']}}</option>						
						@endforeach
					</select>
					@if($errors->has('default_certificate'))
						<div class="invalid-feedback">
							{{ $errors->first('default_certificate') }}
						</div>
					@endif
					<span class="help-block">{{ trans('cruds.employee.fields.default_certificate_helper') }}</span>
				</div>
				
				<div class="form-group col-md-4  @if(Auth::user()->department == 'Strategy Department') d-none @endif">
					<label for="issue_date">{{ trans('global.courseduration') }}</label>
					<input class="form-control  {{ $errors->has('courseduration') ? 'is-invalid' : '' }}" type="text" name="courseduration" id="courseduration" value="{{ old('courseduration', $employee->courseduration) }}">
					@if($errors->has('issue_date'))
						<div class="invalid-feedback">
							{{ $errors->first('issue_date') }}
						</div>
					@endif
					<span class="help-block">{{ trans('cruds.employee.fields.issue_date_helper') }}</span>
				</div>										
			</div>
			
			<div class="form-row">
				<div class="form-group col-md-4">
					<label class="required">{{ trans('cruds.employee.fields.emp_category') }}</label>
					<select class="form-control {{ $errors->has('emp_category') ? 'is-invalid' : '' }}" name="emp_category" id="emp_category" required>
						<option value disabled {{ old('emp_category', null) === null ? 'selected' : '' }}>{{ trans('global.pleaseSelect') }}</option>
						@foreach(App\Employee::EMP_CATEGORY_SELECT as $key => $label)
							<option value="{{ $key }}" {{ old('emp_category', $employee->emp_category) === (string) $key ? 'selected' : '' }}>{{ $label }}</option>
						@endforeach
					</select>
					@if($errors->has('emp_category'))
						<div class="invalid-feedback">
							{{ $errors->first('emp_category') }}
						</div>
					@endif
					<span class="help-block">{{ trans('cruds.employee.fields.emp_category_helper') }}</span>
				</div>
				<div class="form-group col-md-4">
					<label class="required" for="first_name">{{ trans('cruds.employee.fields.first_name') }}</label>
					<input class="form-control {{ $errors->has('first_name') ? 'is-invalid' : '' }}" type="text" name="first_name" id="first_name" value="{{ old('first_name', $employee->first_name) }}" required>
					@if($errors->has('first_name'))
						<div class="invalid-feedback">
							{{ $errors->first('first_name') }}
						</div>
					@endif
					<span class="help-block">{{ trans('cruds.employee.fields.first_name_helper') }}</span>
				</div>
				<div class="form-group col-md-4">
					<label for="last_name">{{ trans('cruds.employee.fields.last_name') }}</label>
					<input class="form-control {{ $errors->has('last_name') ? 'is-invalid' : '' }}" type="text" name="last_name" id="last_name" value="{{ old('last_name', $employee->last_name) }}">
					@if($errors->has('last_name'))
						<div class="invalid-feedback">
							{{ $errors->first('last_name') }}
						</div>
					@endif
					<span class="help-block">{{ trans('cruds.employee.fields.last_name_helper') }}</span>
				</div>
			</div>
			
			<div class="form-row">
				<div class="form-group col-md-4">
					<label for="issue_date">{{ trans('cruds.employee.fields.issue_date') }}</label>
					<input class="form-control date {{ $errors->has('issue_date') ? 'is-invalid' : '' }}" type="text" name="issue_date" id="issue_date" value="{{ old('issue_date', $employee->issue_date) }}">
					@if($errors->has('issue_date'))
						<div class="invalid-feedback">
							{{ $errors->first('issue_date') }}
						</div>
					@endif
					<span class="help-block">{{ trans('cruds.employee.fields.issue_date_helper') }}</span>
				</div>
				<div class="form-group col-md-4">
					<label for="serial_no">{{ trans('cruds.employee.fields.serial_no') }}</label>
					<input class="form-control {{ $errors->has('serial_no') ? 'is-invalid' : '' }}" type="text" name="serial_no" id="serial_no" value="{{ old('serial_no', $employee->serial_no) }}">
					@if($errors->has('serial_no'))
						<div class="invalid-feedback">
							{{ $errors->first('serial_no') }}
						</div>
					@endif
					<span class="help-block">{{ trans('cruds.employee.fields.serial_no_helper') }}</span>
				</div>
				
				<div class="form-group col-md-4">
					<label>{{ trans('cruds.employee.fields.certificate_approval_status') }}</label>
					<?php /*<select  disabled  class="form-control {{ $errors->has('certificate_approval_status') ? 'is-invalid' : '' }}" name="certificate_approval_status" id="certificate_approval_status">
						<option value disabled {{ old('certificate_approval_status', null) === null ? 'selected' : '' }}>
							{{ trans('global.pleaseSelect') }}
						</option>
						@foreach(App\Employee::CERTIFICATE_APPROVAL_STATUS_SELECT as $key => $label)
							<option value="{{ $key }}" {{ old('certificate_approval_status', $employee->certificate_approval_status) === (string) $key ? 'selected' : '' }}>
								{{ $label }}
							</option>
						@endforeach
					</select>*/ ?>
					
					<select class="form-control {{ $errors->has('certificate_approval_status') ? 'is-invalid' : '' }}" name="certificate_approval_status" id="certificate_approval_status">
						@foreach(App\Employee::CERTIFICATE_APPROVAL_STATUS_SELECT as $key => $label)	
							@if(Auth::user()->roles[0]->title == 'Admin' || Auth::user()->roles[0]->title == 'Principal')
								<option value="{{ $key }}" {{ old('certificate_approval_status', '4') === (string) $key ? 'selected' : '' }} >
									{{ $label }}
								</option>																		
							@elseif(Auth::user()->roles[0]->title == 'HOD')
								@if(in_array($key,array(4)))
									<option value="{{ $key }}" {{ old('certificate_approval_status', '4') === (string) $key ? 'selected' : '' }} >
										{{ $label }}
									</option>
								@endif
							@elseif(Auth::user()->roles[0]->title == 'Manager')
								@if(in_array($key,array(1)))
									<option value="{{ $key }}" {{ old('certificate_approval_status', '1') === (string) $key ? 'selected' : '' }}>
										{{ $label }}
									</option>
								@endif
							@elseif(Auth::user()->roles[0]->title == 'Officer')
								@if(in_array($key,array(1)))
									<option value="{{ $key }}" {{ old('certificate_approval_status', '1') === (string) $key ? 'selected' : '' }}>
										{{ $label }}
									</option>
								@endif
							@endif
						@endforeach
					</select>
					@if($errors->has('certificate_approval_status'))
						<div class="invalid-feedback">
							{{ $errors->first('certificate_approval_status') }}
						</div>
					@endif
					<span class="help-block">{{ trans('cruds.employee.fields.certificate_approval_status_helper') }}</span>
				</div>			
			</div>
			
			<div class="form-row">						
				<div class="form-group  col-md-4 @if($employee->employee_type == 2){{ 'd-none' }}@endif">
					<label for="emp_id">{{ trans('cruds.employee.fields.emp_id') }}</label>
					<input class="form-control {{ $errors->has('emp_id') ? 'is-invalid' : '' }}" type="text" name="emp_id" id="emp_id" value="{{ old('emp_id', $employee->emp_id) }}">
					@if($errors->has('emp_id'))
						<div class="invalid-feedback">
							{{ $errors->first('emp_id') }}
						</div>
					@endif
					<span class="help-block">{{ trans('cruds.employee.fields.emp_id_helper') }}</span>
				</div>	
				<div class="form-group col-md-4 @if($employee->employee_type != 2){{ 'd-none' }}@endif">
					<label for="institution_name">{{ trans('cruds.employee.fields.institution_name') }}</label>
					<input class="form-control {{ $errors->has('institution_name') ? 'is-invalid' : '' }}" type="text" name="institution_name" id="institution_name" value="{{ old('institution_name', $employee->institution_name) }}">
					@if($errors->has('institution_name'))
						<div class="invalid-feedback">
							{{ $errors->first('institution_name') }}
						</div>
					@endif
					<span class="help-block">{{ trans('cruds.employee.fields.institution_name_helper') }}</span>
				</div>
				<div class="form-group col-md-4">
					<label for="emailid">{{ trans('cruds.employee.fields.emailid') }}</label>
					<input class="form-control {{ $errors->has('emailid') ? 'is-invalid' : '' }}" type="email" name="emailid" id="emailid" value="{{ old('emailid', $employee->emailid) }}" required>
					@if($errors->has('emailid'))
						<div class="invalid-feedback">
							{{ $errors->first('emailid') }}
						</div>
					@endif
					<span class="help-block">{{ trans('cruds.employee.fields.last_name_helper') }}</span>
				</div>			
			</div>
			
			<div class="form-row">
				<div class="form-group col-md-12">
					<label for="emp_id">{{ trans('global.pdfsetup') }}</label>
					<input class="form-control {{ $errors->has('pdf_name_setup') ? 'is-invalid' : '' }}" type="text" name="pdf_name_setup" id="pdf_name_setup" value="{{ old('emp_id', $employee->pdf_name_setup ) }}">					
				</div>			
			</div>			
            <div class="form-group d-none">
                <label class="required" for="certificate_approve_by">{{ trans('cruds.employee.fields.certificate_approve_by') }}</label>
                <input class="form-control {{ $errors->has('certificate_approve_by') ? 'is-invalid' : '' }}" type="number" name="certificate_approve_by" id="certificate_approve_by" value="{{ old('certificate_approve_by', $employee->certificate_approve_by) }}" step="1" required>
                @if($errors->has('certificate_approve_by'))
                    <div class="invalid-feedback">
                        {{ $errors->first('certificate_approve_by') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.employee.fields.certificate_approve_by_helper') }}</span>
            </div>
            <div class="form-group d-none">
                <label for="certificate_approve_date">{{ trans('cruds.employee.fields.certificate_approve_date') }}</label>
                <input class="form-control date {{ $errors->has('certificate_approve_date') ? 'is-invalid' : '' }}" type="text" name="certificate_approve_date" id="certificate_approve_date" value="{{ old('certificate_approve_date', $employee->certificate_approve_date) }}">
                @if($errors->has('certificate_approve_date'))
                    <div class="invalid-feedback">
                        {{ $errors->first('certificate_approve_date') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.employee.fields.certificate_approve_date_helper') }}</span>
            </div>
            
            <div class="form-group d-none">
                <label>{{ trans('cruds.employee.fields.employee_type') }}</label>
                <select class="form-control {{ $errors->has('employee_type') ? 'is-invalid' : '' }}" name="employee_type" id="employee_type">
                    <option value disabled {{ old('employee_type', null) === null ? 'selected' : '' }}>{{ trans('global.pleaseSelect') }}</option>
                    @foreach(App\Employee::EMPLOYEE_TYPE_SELECT as $key => $label)
                        <option value="{{ $key }}" {{ old('employee_type', $employee->employee_type) === (string) $key ? 'selected' : '' }}>{{ $label }}</option>
                    @endforeach
                </select>
                @if($errors->has('employee_type'))
                    <div class="invalid-feedback">
                        {{ $errors->first('employee_type') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.employee.fields.employee_type_helper') }}</span>
            </div>
            <div class="form-group d-none">
                <label for="created_by">{{ trans('cruds.employee.fields.created_by') }}</label>
                <input class="form-control {{ $errors->has('created_by') ? 'is-invalid' : '' }}" type="number" name="created_by" id="created_by" value="{{ old('created_by', $employee->created_by) }}" step="1">
                @if($errors->has('created_by'))
                    <div class="invalid-feedback">
                        {{ $errors->first('created_by') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.employee.fields.created_by_helper') }}</span>
            </div>
            <div class="form-group d-none">
                <label for="certificate_qrcode">{{ trans('cruds.employee.fields.certificate_qrcode') }}</label>
                <textarea class="form-control {{ $errors->has('certificate_qrcode') ? 'is-invalid' : '' }}" readonly name="certificate_qrcode" id="certificate_qrcode">{{ old('certificate_qrcode', $employee->certificate_qrcode) }}</textarea>
                @if($errors->has('certificate_qrcode'))
                    <div class="invalid-feedback">
                        {{ $errors->first('certificate_qrcode') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.employee.fields.certificate_qrcode_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection