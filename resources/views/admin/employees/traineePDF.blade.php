<?php
$data 	= file_get_contents('images/traineeCertificate10.jpg');//traineeCertificate_bk_02082020
$base64 = 'data:image/jpg;base64,' . base64_encode($data);
?>
<!DOCTYPE html>
<html dir="rtl" lang="ar">
<head>
<title>{{$certificate['certificate_title']}}</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<style>
body{
	background: url(<?php echo $base64; ?>);
	font-family: DejaVu Sans, sans-serif;
	margin-header:0mm;
	<?php /*
	
	direction: rtl;
	text-align: center;	*/?>
}
@page {
	margin:1px;
}
</style>
</head>
<body>
<?php /* <p align="center" style="font-size:30px; text-align:center; width:750px; margin:1px 1px 1px 220px; padding:10px 1px 0px 10px; border:0px solid #F1F;"> */ ?>
@if(0)
	<p style="font-size:30px; color:#BF9140; text-align:right; width:650px; margin:330px auto 0px; border:0px solid #FF0;">
	دبي بخالص الشكر والتقدير إلى
	</p>
	<p align="center" style="font-size:30px; text-align:center; width:830px; margin:1px 1px 1px 220px; padding:10px 1px 0px 10px; border:1px solid #F1F;">
		محمد بن سلمان بن عبدالعزيز آل سعود محمد بنسلمان بن عبدالعزيز آل سعود	
	</p>
	@if(1)
	<p align="center" style="display:none; font-size:30px; text-align:center; width:800px; margin:1px 1px 1px 220px; padding:10px 1px 0px 10px; border:1px solid #F1F;">
		محمد بن سلمان بن عبدالعزيز آل سعو
	</p>
	@else
	<p style="font-size:20px; font-color:#F00; text-align:center; width:650px; margin:5px auto 0px; padding-left: 50px; border:0px solid #FF0;">
	قد اجتاز دورة الحس الأمني
	</p>
	@endif
	<p style="font-size:20px; font-color:#F00; text-align:center; width:650px; margin:1px auto 0px; padding-left: 120px; border:0px solid #FF0;">
	والمنعقدة في فندق الحبتور بولو۔ دہی
	</p>
	<p style="font-size:20px; font-color:#F00; font-family:sans-serif;; text-align:center; width:650px; margin:1px auto 0px; padding-left: 10px; border:0px solid #FF0;">
	11/07/2020 - 30/08/2020 <?php /*from_date - to_date*/ ?>
	</p>
	<p style="font-size:20px; font-color:#F00; text-align:center; width:650px; margin:1px auto 0px; padding-left: 100px; border:0px solid #FF0;">
	مع تمنياتام بالتوفيق
	</p>
@else
	{!!str_replace('emp_category','',str_replace('first_name',$employee['first_name'],str_replace('last_name',$employee['last_name'],str_replace('from_date - to_date',$employee['courseduration'],str_replace('pdf_name_setup',$employee['pdf_name_setup'],$certificate['certificate_details'])))))!!}
@endif
	
	@if(isset($employee['certificate_approval_status']) && $employee['certificate_approval_status'] == 2)
			<img src="{{ asset('images/qrcode/qrcode-').$qrstr }}.png" width="180" style="float:right; margin-top:20px; margin-right:215px;  border:2px solid #f00;" />	
			<div style="font-size:18px; margin-top:75px; margin-left:350px;">{{$certificate['certificate_issue_by_post']}} /</div>
			<div style="font-size:18px; margin-top:1px; margin-left:250px;">{{$certificate['certificate_issue_by_officername']}}</div>
			<div style="font-size:18px; margin-top:1px; margin-left:270px;">{{$certificate['certificate_issue_msg']}}</div>		
			<?php /* */ ?>
	@endif
</body>
</html>

