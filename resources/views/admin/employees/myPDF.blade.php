<?php
$data 	= file_get_contents('images/certificate_03.jpg');
$base64 = 'data:image/jpg;base64,' . base64_encode($data);

/*
شهادة تكريم
تتقدم الإدارة العامة للدفاع المدني - دبي بخالص الشكر والتقدير إلى
مدني / سید کا شف ء تتشا تشسسش 0110
تقديرا لجهوده الفعالة و تقديم اقتراحا مجديا يسهم
في تطوير العمل بما يحقق الأهداف الاستراتيجية
مع تمنياتنا له بالتوفيق والنجاح
العميد سيد ششکا سشيا تنا ۱

*/

?>
<!DOCTYPE html>
<html>
<head>
<title>{{$certificate['certificate_title']}}</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<style>
body{
	background: url(<?php echo $base64 ?>);
	margin-header:0mm;
}
@page {
margin:1px;
}
<?php /*


body{
	font-family: DejaVu Sans, sans-serif;
	background: url('images/certificate_bgimage_test.jpg') no-repeat 0 0;
    background-image-resize: 6;	
	margin-header:0mm;
}

@page {
	margin-header: 5mm;
}

background: url('images/certificate_bgimage_test.jpg') no-repeat 0 0;
@page { sheet-size: A3-L; }
@page bigger { sheet-size: 3348mm 2317mm; }
@page toc { sheet-size: A4; }


*/?>
</style>
</head>
<body>
<table width="100%" align="center" border="0" style="margin-top:125px;">
	<tr>
		<th align="center">
			<img src="{{ public_path() }}\images\certificate_logo.png" width="2300"  />
		</th>
	</tr>
</table>

<div style="height:1470px; border:1px solid #000;">
	<h2 align="center" style="font-size:90px; margin-top:240px; color:#BF9140">{{$certificate['certificate_title']}}</h2>
	<h3 align="center" style="font-size:80px; margin-top:90px; color:#000">{{$certificate['certificate_subtitle']}}</h3>
	<h2 align="center" style="font-size:80px; margin-top:105px; color:#000;">110{{$employee['emp_category'].' '.$employee['first_name'].' '.$employee['last_name']}}</h2>
	<div style="font-size:80px; line-height:100px; text-align:center; width:60%; margin:100px auto; border:1px soild #F00;" align="center">
		{{strip_tags($certificate['certificate_details'])}}
	</div>
	<div style="font-size:80px; line-height:100px; text-align:center; width:60%; margin:100px auto; border:1px soild #F00;" align="center">
		{{strip_tags($certificate['certificate_details1'])}}
	</div>
	<div style="font-size:80px; line-height:100px; text-align:center; width:60%; margin:100px auto; border:1px soild #F00;" align="center">
		{{strip_tags($certificate['certificate_details2'])}}
	</div>
</div>

@if(isset($employee['certificate_approve_by']) && $employee['certificate_approve_by']>0)
<table width="81%" align="center" border="1"  style="margin-top:10px;">	
	<tr>
		<td>
			<h1 style="font-size:50px; margin-top:47px;">{{$user[$employee['certificate_approve_by']]['signature_name']}} / {{$user[$employee['certificate_approve_by']]['name']}}</h1>
			<h1 style="font-size:50px; margin-top:35px;">{{$user[$employee['certificate_approve_by']]['department']}}</h1>
		</td>
		<td align="right">		
			<img src="{{ public_path() }}\images\qrcode\qrcode-{{$qrstr}}.png" width="200" />
		</td>
	</tr>
</table>
@endif


</body>
</html>

