@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('cruds.internalmember.title_singular') }} {{ trans('global.list') }}
		
		
		@can('internalmember_create')
			<a class="btn btn-success float-right mr-2" href="{{ route('admin.internalmembers.create') }}">
				{{ trans('global.add') }} <?php /*{{ trans('cruds.internalmember.title_singular') }}*/?>
			</a>	
			@if(Auth::user()->user_type)
				<button class="btn btn-warning float-right" style="margin-right:10px;margin-left:10px;" data-toggle="modal" data-target="#csvImportModal">
					{{ trans('global.app_csvImport') }}
				</button>
				@include('csvImport.modal', ['model' => 'Internalmember', 'route' => 'admin.internalmembers.parseCsvImport'])
			@endif
			<?php /*@include('csvImport.modal', ['model' => 'Member', 'route' => 'admin.employees.parseCsvImport'])	*/?>
		@endcan				
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-Internalmember">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('cruds.internalmember.fields.id') }}
                        </th>
						<?php /*
                        <th>
                            {{ trans('global.ctype') }}
                        </th>  */ ?>                          
						<th>
                            {{ trans('cruds.internalmember.fields.emp_id') }}
                        </th>
                        <th>
                            {{ trans('cruds.internalmember.fields.emp_category') }}
                        </th>
                        <th>
                            {{ trans('cruds.internalmember.fields.first_name') }}
                        </th>
                        <th>
                            {{ trans('cruds.internalmember.fields.last_name') }}
                        </th>
						<?php /*
                        <th>
                            {{ trans('cruds.internalmember.fields.institution_name') }}
                        </th>*/ ?>
                        <th>
                            {{ trans('cruds.internalmember.fields.emailid') }}
                        </th>
                        <th>
                            {{ trans('cruds.internalmember.fields.contact_no') }}
                        </th>
                        <th>
                            {{ trans('cruds.internalmember.fields.department') }}
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($internalmembers as $key => $internalmember)
                        <tr data-entry-id="{{ $internalmember->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $internalmember->id ?? '' }}
                            </td>
							<?php /*
						<td>
								@if($internalmember->emp_type == 1)
									General
								@elseif($internalmember->emp_type == 2)
									Strategy
								@elseif($internalmember->emp_type == 3)
									Trainee
								@endif
                        </td>	*/?>						
                            <td>
                                {{ $internalmember->emp_id ?? '' }}
                            </td>
                            <td>
                                {{ App\Internalmember::EMP_CATEGORY_SELECT[$internalmember->emp_category] ?? '' }}
                            </td>
                            <td>
                                {{ $internalmember->first_name ?? '' }}
                            </td>
                            <td>
                                {{ $internalmember->last_name ?? '' }}
                            </td>
							<?php /*
                            <td>
                                {{ $internalmember->institution_name ?? '' }}
                            </td>*/ ?>
                            <td>
                                {{ $internalmember->emailid ?? '' }}
                            </td>
                            <td>
                                {{ $internalmember->contact_no ?? '' }}
                            </td>
                            <td>
                                {{ $internalmember->department ?? '' }}
                            </td>
                            <td>
                                @can('internalmember_show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.internalmembers.show', $internalmember->id) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                @endcan

                                @can('internalmember_edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.internalmembers.edit', $internalmember->id) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan

                                @can('internalmember_delete')
                                    <form action="{{ route('admin.internalmembers.destroy', $internalmember->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan

                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>



@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('internalmember_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.internalmembers.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    orderCellsTop: true,
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
  let table = $('.datatable-Internalmember:not(.ajaxTable)').DataTable({ buttons: dtButtons })
  $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
      $($.fn.dataTable.tables(true)).DataTable()
          .columns.adjust();
  });
  
})

</script>
@endsection