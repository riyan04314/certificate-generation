@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('cruds.user.title_singular') }} {{ trans('global.list') }}
		
		
@can('user_create')
            <a class="btn btn-success float-right" href="{{ route('admin.users.create') }}">
                {{ trans('global.add') }} <?php /*{{ trans('cruds.user.title_singular') }}*/?>
            </a>
@endcan		
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('cruds.user.fields.id') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.name') }}
                        </th>
						<?php /*
						<th>
                            {{ trans('global.type') }}
                        </th>*/?>
                        <th>
                            {{ trans('cruds.user.fields.roles') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.email') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.department') }}
                        </th>
                        <th>Created By</th>
                        <?php /*<th>
                            {{ trans('cruds.user.fields.signature') }}
                        </th>*/ ?>
						<?php /*
						@if(in_array(Auth::user()->roles[0]->title,array('Admin')))
                        <th>
                            {{ trans('cruds.user.fields.signature_name') }}
                        </th>
						
						@endif*/ ?>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $key => $user)
						@if(Auth::user()->roles[0]->title == 'Admin' || Auth::user()->roles[0]->title == 'Principal' || $user->created_by == Auth::user()->id)
                        <tr data-entry-id="{{ $user->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $user->id ?? '' }}
                            </td>
                            <td>
                                {{ $user->name ?? '' }}
                            </td>
							<?php /*
                            <td>
								@if($user->user_type == 1)
									General
								@elseif($user->user_type == 2)
									Strategy
								@elseif($user->user_type == 3)
									Trainee
								@endif
                            </td>*/ ?>
                            <td>
                                @foreach($user->roles as $key => $item)
                                    <span class="badge badge-info">{{ $item->title }}</span>
                                @endforeach
                            </td>
							
							<td>
                                {{ $user->email ?? '' }}
                            </td>
                            <td>
                                {{ $user->department ?? '' }}
                            </td>                            
                            <td>
								@if(isset($usersname[$user->created_by]))
									{{ $usersname[$user->created_by] }}
								@endif
                            </td>                            
                            <?php /*<td>
                                @if($user->signature)
                                    <a href="{{ $user->signature->getUrl() }}" target="_blank">
                                        {{ trans('global.view_file') }}
                                    </a>
                                @endif
                            </td>*/ ?>
							<?php /*
							@if(in_array(Auth::user()->roles[0]->title,array('Admin')))
                            <td>
                                {{ $user->signature_name ?? '' }}
                            </td>
							@endif
							*/ ?>
                            <td>							
                                @can('user_show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.users.show', $user->id) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                @endcan

                                @can('user_edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.users.edit', $user->id) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan
								
								@if($user->id != 1)
									@can('user_delete')
										<form action="{{ route('admin.users.destroy', $user->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
											<input type="hidden" name="_method" value="DELETE">
											<input type="hidden" name="_token" value="{{ csrf_token() }}">
											<input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
										</form>
									@endcan
								@endif
                            </td>

                        </tr>
						@endif
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>



@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('user_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.users.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    orderCellsTop: true,
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
  let table = $('.datatable-User:not(.ajaxTable)').DataTable({ buttons: dtButtons })
  $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
      $($.fn.dataTable.tables(true)).DataTable()
          .columns.adjust();
  });
  
})

</script>
@endsection