@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.trainee.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.trainees.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.trainee.fields.id') }}
                        </th>
                        <td>
                            {{ $trainee->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.trainee.fields.emp_category') }}
                        </th>
                        <td>
                            {{ App\Trainee::EMP_CATEGORY_SELECT[$trainee->emp_category] ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.trainee.fields.first_name') }}
                        </th>
                        <td>
                            {{ $trainee->first_name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.trainee.fields.last_name') }}
                        </th>
                        <td>
                            {{ $trainee->last_name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.trainee.fields.institution_name') }}
                        </th>
                        <td>
                            {{ $trainee->institution_name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.trainee.fields.emailid') }}
                        </th>
                        <td>
                            {{ $trainee->emailid }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.trainee.fields.contact_no') }}
                        </th>
                        <td>
                            {{ $trainee->contact_no }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.trainee.fields.issue_date') }}
                        </th>
                        <td>
                            {{ $trainee->issue_date }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.trainee.fields.serial_no') }}
                        </th>
                        <td>
                            {{ $trainee->serial_no }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.trainee.fields.certificate_approval_status') }}
                        </th>
                        <td>
                            {{ App\Trainee::CERTIFICATE_APPROVAL_STATUS_SELECT[$trainee->certificate_approval_status] ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.trainee.fields.certificate_approve_by') }}
                        </th>
                        <td>
                            {{ $trainee->certificate_approve_by }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.trainee.fields.certificate_approve_date') }}
                        </th>
                        <td>
                            {{ $trainee->certificate_approve_date }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.trainee.fields.certificate_qrcode') }}
                        </th>
                        <td>
                            {{ $trainee->certificate_qrcode }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.trainee.fields.default_certificate') }}
                        </th>
                        <td>
                            {{ $trainee->default_certificate }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.trainee.fields.created_by') }}
                        </th>
                        <td>
                            {{ $trainee->created_by }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.trainees.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection