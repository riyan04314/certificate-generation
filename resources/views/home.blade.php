<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('images/sitelogo.jpg')}}">
     <title>{{ trans('panel.site_title') }}</title>
    <!-- Custom CSS -->
    <link href="{{asset('/mendy-admin/assets/extra-libs/jvector/jquery-jvectormap-2.0.2.css')}}" rel="stylesheet" />
    <link href="{{asset('/mendy-admin/assets/libs/chartist/dist/chartist.min.css')}}" rel="stylesheet">
    <link href="{{asset('/mendy-admin/assets/libs/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
    <!-- Custom CSS -->
	
<link href="{{asset('/mendy-admin/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css')}}" rel="stylesheet">	
	
    <link href="{{asset('/mendy-admin/dist/css/style.min.css')}}" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="{{asset('js/html5shiv.js')}}"></script>
    <script src="{{asset('js/respond.min.js')}}"></script>
<![endif]-->
<link href="{{asset('/mendy-admin/dist/css/customstyle.css')}}" rel="stylesheet">

</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <div class="navbar-header">
                    <!-- This is for the sidebar toggle which is visible on mobile only -->
                    <a class="nav-toggler d-block d-md-none" href="javascript:void(0)">
                        <i class="ti-menu ti-close"></i>
                    </a>
                    <!-- ============================================================== -->
                    <!-- Logo -->
                    <!-- ============================================================== -->
                    <div class="navbar-brand">
                        <a href="index.html" class="logo">
                            <!-- Logo icon -->
                            <b class="logo-icon">
                                <?php /*<!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                                <!-- Dark Logo icon -->
                                <img src="{{asset('images/sitelogo.jpg')}}" width="40" alt="homepage" class="dark-logo" />
                                <!-- Light Logo icon -->
                                <img src="{{asset('images/sitelogo.jpg')}}" width="40"  alt="homepage" class="light-logo" />*/ ?>
                            </b>
                            <!--End Logo icon -->
                            <!-- Logo text -->
                            <span class="logo-text">
                                <?php /*<!-- dark Logo text -->
                                <img src="{{asset('images/sitelogo.jpg')}}" alt="homepage" class="dark-logo" />
                                <!-- Light Logo text -->
                                <img src="{{asset('images/sitelogo.jpg')}}" class="light-logo" alt="homepage" />*/ ?>
                            </span>
                        </a>
                        <a class="sidebartoggler d-none d-lg-block" href="javascript:void(0)" data-sidebartype="mini-sidebar">
                            <i data-feather="menu"></i>
                        </a>
                    </div>
                    <!-- ============================================================== -->
                    <!-- End Logo -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- Toggle which is visible on mobile only -->
                    <!-- ============================================================== -->
                    <a class="topbartoggler d-block d-md-none" href="javascript:void(0)" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                        aria-label="Toggle navigation">
                        <i class="ti-more"></i>
                    </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse collapse" id="navbarSupportedContent">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav float-left mr-auto">
                        <li class="nav-item dropdown mega-dropdown">
                            <a class="nav-link dropdown-toggle waves-effect waves-dark" href="#" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                            </a>
                        </li>
                    </ul>
                    <!-- ============================================================== -->
                    <!-- Right side toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav float-right">
                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle waves-effect waves-dark pro-pic" href="#" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                                <span class="mr-3 font-normal d-none d-sm-inline-block pro-name"><span>Hello,</span>
								{{Auth::user()->name}}</span>
                                <img src="{{asset('images/sitelogo.jpg')}}" alt="user" class="rounded-circle" width="40">
                            </a>
                            <div class="dropdown-menu dropdown-menu-right user-dd animated flipInY">
                                <div class="d-flex no-block align-items-center p-3 bg-light mb-2">
                                    <div class="">
                                        <img src="{{asset('images/sitelogo.jpg')}}" alt="user" class="rounded-circle"
                                            width="60">
                                    </div>
                                    <div class="ml-2">                                       
                                        <h4 class="mb-0">{{ Auth::user()->department}}</h4>
                                        <p class="mb-0">{{ Auth::user()->email }}</p>																				
                                    </div>
                                </div>
                                <div class="profile-dis scrollable">
                                    <?php /*<a class="dropdown-item" href="javascript:void(0)">
                                        <i class="ti-user mx-1"></i> My Profile</a>
                                    <a class="dropdown-item" href="javascript:void(0)">
                                        <i class="ti-wallet mx-1"></i> My Balance</a>
                                    <a class="dropdown-item" href="javascript:void(0)">
                                        <i class="ti-email mx-1"></i> Inbox</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="javascript:void(0)">
                                        <i class="ti-settings mx-1"></i> Account Setting</a>*/ ?>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="javascript:void(0)"  onclick="event.preventDefault(); document.getElementById('logoutfrm').submit();">
                                        <i class="fa fa-power-off mx-1"></i> Logout</a>										
                                </div>
                            </div>
                        </li>
                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
       @include('partials.menu')
	   <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
		<style>

		</style>
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb bg-gradient-primary pb-5">
                <h4 class="text-white">Dashboard</h4>
                <div class="row align-items-center sales-chart py-5">
                    <div class="col-lg-8 pr-lg-5 chart">
                        <div class="pro-sales-2 ct-charts"></div>
                    </div>
                    <div class="col-lg-4 pl-lg-5 mt-4 mt-lg-0">
                        <ul class="list-unstyled">
                            <li class="d-flex align-items-center pb-4 stats">
                                <div>
                                    <span class="d-block font-22 op-5 brwColor" >{{ trans('global.totalemployee') }}</span>
                                    <h3 style="color:#b78536;" class="mb-0 text-white">{{$data['totalemployee']}}</h3>
                                </div>
                                <i class="text-white ml-auto" data-feather="briefcase"></i>
                            </li>
                            <li class="d-flex align-items-center mt-4">
                                <div>
                                    <span class="d-block font-22 op-5 brwColor">{{ trans('global.pending') }}</span>
                                    <h3 class="mb-0 text-white">{{$data['totalpending']}}</
                                </div>
                                <i class="text-white ml-auto" data-feather="cloud"></i>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid negative-margin" <?php /*style="background-color:#DBC186;"*/ ?>>
                <!-- ============================================================== -->
                <!-- Sales -->
                <!-- ============================================================== -->
<div class="row">
                    <div class="col-md-6 col-lg-3">
                        <div class="card">
                            <div class="card-body" style="border-radius: 16px;background: #f4d3a6; -webkit-box-shadow: 0px 10px 14px -8px rgba(0,0,0,0.59);
-moz-box-shadow: 0px 10px 14px -8px rgba(0,0,0,0.59);
box-shadow: 0px 10px 14px -8px rgba(0,0,0,0.59);">
                                <div class="d-flex align-items-center">
                                    <div>
                                        <span class="d-block text-dark op-7 font-medium" style="color:#50463a !important">{{ trans('global.totalemployee') }}</span>
                                        <h3 class="mb-0">{{$data['totalemployee']}}</h3>
                                    </div>
                                    <div class="round round-purple ml-auto" style="border:2px solid #fff;"><i data-feather="shield"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-3">
                        <div class="card">
                            <div class="card-body" style="border-radius: 16px;background: #ecc27d; -webkit-box-shadow: 0px 10px 14px -8px rgba(0,0,0,0.59);
-moz-box-shadow: 0px 10px 14px -8px rgba(0,0,0,0.59);
box-shadow: 0px 10px 14px -8px rgba(0,0,0,0.59);">
                                <div class="d-flex align-items-center">
                                    <div>
                                        <span class="d-block text-dark op-7 font-medium" style="color:#50463a !important">{{ trans('global.pending') }}</span>
                                        <h3 class="mb-0">{{$data['totalpending']}}</h3>
                                    </div>
                                    <div class="round round-primary ml-auto" style="border:2px solid #fff;"><i data-feather="user-plus"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-3">
                        <div class="card">
                            <div class="card-body" style="border-radius: 16px;background: #e8ddc2; -webkit-box-shadow: 0px 10px 14px -8px rgba(0,0,0,0.59);
-moz-box-shadow: 0px 10px 14px -8px rgba(0,0,0,0.59);
box-shadow: 0px 10px 14px -8px rgba(0,0,0,0.59);">
                                <div class="d-flex align-items-center">
                                    <div>
                                        <span class="d-block text-dark op-7 font-medium" style="color:#50463a !important">{{ trans('global.reviewed') }}</span>
                                        <h3 class="mb-0">{{$data['totalreviewed']}}</h3>
                                    </div>
                                    <div class="round round-info ml-auto" style="border:2px solid #fff;"><i data-feather="cloud"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-3">
                        <div class="card">
                            <div class="card-body" style="border-radius: 16px;background: #d6b66e; -webkit-box-shadow: 0px 10px 14px -8px rgba(0,0,0,0.59);
-moz-box-shadow: 0px 10px 14px -8px rgba(0,0,0,0.59);
box-shadow: 0px 10px 14px -8px rgba(0,0,0,0.59);">
                                <div class="d-flex align-items-center">
                                    <div>
                                        <span class="d-block text-dark op-7 font-medium" style="color:#50463a !important">{{ trans('global.approved') }}</span>
                                        <h3 class="mb-0">{{$data['totalapproved']}}</h3>
                                    </div>
                                    <div class="round round-danger ml-auto" style="border:2px solid #fff;"><i data-feather="briefcase"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- Sales -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Download and Bandwidth -->
                <!-- ============================================================== -->
				<?php /*
				<div class="row d-none">
<!-- column -->
                    <div class="col-md-12 col-lg-7">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex align-items-center">
                                    <div>
                                        <h4 class="card-title">Top 10 Best Performers</h4>
                                        <h6 class="card-subtitle mb-0">Risus commodo viverra maecenas accumsan lacus
                                            vel
                                            facilisis. </h6>
                                    </div>
                                    <div class="ml-auto">
                                        <div class="dropdown title-dropdown">
                                            <button class="btn btn-link text-muted dropdown-toggle" type="button" id="dd1"
                                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i data-feather="more-horizontal"></i>
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dd1">
                                                <a class="dropdown-item" href="#">Edit</a>
                                                <a class="dropdown-item" href="#">Delete</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="mt-5" style="height: 205px;">
                                    <canvas id="bar-chart" width="400" height="150"></canvas>
                                </div>
                                <div class="row mt-5 mb-3">
                                    <div class="col-6 col-md-4">
                                        <div class="d-flex align-items-start">
                                            <div>
                                                <i class="text-primary" data-feather="target"></i>
                                            </div>
                                            <div class="ml-2">
                                                <h3 class="mb-0">$3,56799.56</h3>
                                                <span>Total Income</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-md-4">
                                        <div class="d-flex align-items-start">
                                            <div>
                                                <i class="text-primary" data-feather="trending-up"></i>
                                            </div>
                                            <div class="ml-2">
                                                <h3 class="mb-0">$769.08</h3>
                                                <span>Monthly Avg</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-md-4 mt-3 mt-md-0">
                                        <div class="d-flex align-items-start">
                                            <div>
                                                <i class="text-primary" data-feather="shopping-bag"></i>
                                            </div>
                                            <div class="ml-2">
                                                <h3 class="mb-0">5489</h3>
                                                <span>Total Sales</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    				
				</div>*/ ?>
				
				
			
				<div class="row d-none">						
					<div class="col-md-6 col-lg-6">
						<div id="chartContainerA" style="height: 300px; width: 100%; background-color:#CCC"></div>
						<br><br>
					</div>
					<div class="col-md-6 col-lg-6">
						<div id="chartContainer" style="height: 300px; width: 100%; background-color:#CCC"></div>
						<br><br>
					</div>
					<div class="col-md-6 col-lg-6">
						<div id="chartContainerB" style="height: 300px; width: 100%; background-color:#CCC"></div>
						<br><br>
					</div>
					<div class="col-md-6 col-lg-6">
						<div id="chartContainerC" style="height: 300px; width: 100%; background-color:#CCC"></div>
						<br><br>
					</div>
					
				</div>				
				
				
				
				
			
			</div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer text-center">
                All Rights Reserved by Us.
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- customizer Panel -->
    <!-- ============================================================== -->
    <aside class="customizer">
        <a href="javascript:void(0)" class="service-panel-toggle"><i class="fa fa-spin fa-cog"></i></a>
        <div class="customizer-body">
            <ul class="nav customizer-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab"
                        aria-controls="pills-home" aria-selected="true"><i class="mdi mdi-wrench font-20"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#chat" role="tab" aria-controls="chat"
                        aria-selected="false"><i class="mdi mdi-message-reply font-20"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab"
                        aria-controls="pills-contact" aria-selected="false"><i class="mdi mdi-star-circle font-20"></i></a>
                </li>
            </ul>
            <div class="tab-content" id="pills-tabContent">
                <!-- Tab 1 -->
                <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                    <div class="p-15 border-bottom">
                        <!-- Sidebar -->
                        <h5 class="font-medium m-b-10 m-t-10">Layout Settings</h5>
                        <div class="custom-control custom-checkbox m-t-10">
                            <input type="checkbox" class="custom-control-input sidebartoggler" name="collapssidebar" id="collapssidebar">
                            <label class="custom-control-label" for="collapssidebar">Collapse Sidebar</label>
                        </div>
                        <div class="custom-control custom-checkbox m-t-10">
                            <input type="checkbox" class="custom-control-input" name="sidebar-position" id="sidebar-position">
                            <label class="custom-control-label" for="sidebar-position">Fixed Sidebar</label>
                        </div>
                        <div class="custom-control custom-checkbox m-t-10">
                            <input type="checkbox" class="custom-control-input" name="header-position" id="header-position">
                            <label class="custom-control-label" for="header-position">Fixed Header</label>
                        </div>
                    </div>
                    <div class="p-15 border-bottom">
                        <!-- Logo BG -->
                        <h5 class="font-medium m-b-10 m-t-10">Logo Backgrounds</h5>
                        <ul class="theme-color">
                            <li class="theme-item"><a href="javascript:void(0)" class="theme-link" data-logobg="skin1"></a></li>
                            <li class="theme-item"><a href="javascript:void(0)" class="theme-link" data-logobg="skin2"></a></li>
                            <li class="theme-item"><a href="javascript:void(0)" class="theme-link" data-logobg="skin3"></a></li>
                        </ul>
                        <!-- Logo BG -->
                    </div>
                    <div class="p-15 border-bottom">
                        <!-- Navbar BG -->
                        <h5 class="font-medium m-b-10 m-t-10">Navbar Backgrounds</h5>
                        <ul class="theme-color">
                            <li class="theme-item"><a href="javascript:void(0)" class="theme-link" data-navbarbg="skin1"></a></li>
                            <li class="theme-item"><a href="javascript:void(0)" class="theme-link" data-navbarbg="skin2"></a></li>
                            <li class="theme-item"><a href="javascript:void(0)" class="theme-link" data-navbarbg="skin3"></a></li>
                        </ul>
                        <!-- Navbar BG -->
                    </div>
                    <div class="p-15 border-bottom">
                        <!-- Logo BG -->
                        <h5 class="font-medium m-b-10 m-t-10">Sidebar Backgrounds</h5>
                        <ul class="theme-color">
                            <li class="theme-item"><a href="javascript:void(0)" class="theme-link" data-sidebarbg="skin1"></a></li>
                            <li class="theme-item"><a href="javascript:void(0)" class="theme-link" data-sidebarbg="skin2"></a></li>
                            <li class="theme-item"><a href="javascript:void(0)" class="theme-link" data-sidebarbg="skin3"></a></li>
                        </ul>
                        <!-- Logo BG -->
                    </div>
                </div>
                <!-- End Tab 1 -->
                <!-- Tab 2 -->
                <div class="tab-pane fade" id="chat" role="tabpanel" aria-labelledby="pills-profile-tab">
                    <ul class="mailbox list-style-none m-t-20">
                        <li>
                            <div class="message-center chat-scroll">
                                <a href="javascript:void(0)" class="message-item" id='chat_user_1' data-user-id='1'>
                                    <span class="user-img"> <img src="/mendy-admin/assets/images/users/1.jpg" alt="user" class="rounded-circle">
                                        <span class="profile-status online pull-right"></span> </span>
                                    <div class="mail-contnet">
                                        <h5 class="message-title">Pavan kumar</h5> <span class="mail-desc">Just see the
                                            my admin!</span> <span class="time">9:30 AM</span>
                                    </div>
                                </a>
                                <!-- Message -->
                                <a href="javascript:void(0)" class="message-item" id='chat_user_2' data-user-id='2'>
                                    <span class="user-img"> <img src="/mendy-admin/assets/images/users/2.jpg" alt="user" class="rounded-circle">
                                        <span class="profile-status busy pull-right"></span> </span>
                                    <div class="mail-contnet">
                                        <h5 class="message-title">Sonu Nigam</h5> <span class="mail-desc">I've sung a
                                            song! See you at</span> <span class="time">9:10 AM</span>
                                    </div>
                                </a>
                                <!-- Message -->
                                <a href="javascript:void(0)" class="message-item" id='chat_user_3' data-user-id='3'>
                                    <span class="user-img"> <img src="/mendy-admin/assets/images/users/3.jpg" alt="user" class="rounded-circle">
                                        <span class="profile-status away pull-right"></span> </span>
                                    <div class="mail-contnet">
                                        <h5 class="message-title">Arijit Sinh</h5> <span class="mail-desc">I am a
                                            singer!</span> <span class="time">9:08 AM</span>
                                    </div>
                                </a>
                                <!-- Message -->
                                <a href="javascript:void(0)" class="message-item" id='chat_user_4' data-user-id='4'>
                                    <span class="user-img"> <img src="/mendy-admin/assets/images/users/4.jpg" alt="user" class="rounded-circle">
                                        <span class="profile-status offline pull-right"></span> </span>
                                    <div class="mail-contnet">
                                        <h5 class="message-title">Nirav Joshi</h5> <span class="mail-desc">Just see the
                                            my admin!</span> <span class="time">9:02 AM</span>
                                    </div>
                                </a>
                                <!-- Message -->
                                <!-- Message -->
                                <a href="javascript:void(0)" class="message-item" id='chat_user_5' data-user-id='5'>
                                    <span class="user-img"> <img src="/mendy-admin/assets/images/users/5.jpg" alt="user" class="rounded-circle">
                                        <span class="profile-status offline pull-right"></span> </span>
                                    <div class="mail-contnet">
                                        <h5 class="message-title">Sunil Joshi</h5> <span class="mail-desc">Just see the
                                            my admin!</span> <span class="time">9:02 AM</span>
                                    </div>
                                </a>
                                <!-- Message -->
                                <!-- Message -->
                                <a href="javascript:void(0)" class="message-item" id='chat_user_6' data-user-id='6'>
                                    <span class="user-img"> <img src="/mendy-admin/assets/images/users/6.jpg" alt="user" class="rounded-circle">
                                        <span class="profile-status offline pull-right"></span> </span>
                                    <div class="mail-contnet">
                                        <h5 class="message-title">Akshay Kumar</h5> <span class="mail-desc">Just see
                                            the my admin!</span> <span class="time">9:02 AM</span>
                                    </div>
                                </a>
                                <!-- Message -->
                                <!-- Message -->
                                <a href="javascript:void(0)" class="message-item" id='chat_user_7' data-user-id='7'>
                                    <span class="user-img"> <img src="/mendy-admin/assets/images/users/7.jpg" alt="user" class="rounded-circle">
                                        <span class="profile-status offline pull-right"></span> </span>
                                    <div class="mail-contnet">
                                        <h5 class="message-title">Pavan kumar</h5> <span class="mail-desc">Just see the
                                            my admin!</span> <span class="time">9:02 AM</span>
                                    </div>
                                </a>
                                <!-- Message -->
                                <!-- Message -->
                                <a href="javascript:void(0)" class="message-item" id='chat_user_8' data-user-id='8'>
                                    <span class="user-img"> <img src="/mendy-admin/assets/images/users/8.jpg" alt="user" class="rounded-circle">
                                        <span class="profile-status offline pull-right"></span> </span>
                                    <div class="mail-contnet">
                                        <h5 class="message-title">Varun Dhavan</h5> <span class="mail-desc">Just see
                                            the my admin!</span> <span class="time">9:02 AM</span>
                                    </div>
                                </a>
                                <!-- Message -->
                            </div>
                        </li>
                    </ul>
                </div>
                <!-- End Tab 2 -->
                <!-- Tab 3 -->
                <div class="tab-pane fade p-15" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                    <h6 class="m-t-20 m-b-20">Activity Timeline</h6>
                    <div class="steamline">
                        <div class="sl-item">
                            <div class="sl-left bg-success"> <i class="ti-user"></i></div>
                            <div class="sl-right">
                                <div class="font-medium">Meeting today <span class="sl-date"> 5pm</span></div>
                                <div class="desc">you can write anything </div>
                            </div>
                        </div>
                        <div class="sl-item">
                            <div class="sl-left bg-info"><i class="fas fa-image"></i></div>
                            <div class="sl-right">
                                <div class="font-medium">Send documents to Clark</div>
                                <div class="desc">Lorem Ipsum is simply </div>
                            </div>
                        </div>
                        <div class="sl-item">
                            <div class="sl-left"> <img class="rounded-circle" alt="user" src="/mendy-admin/assets/images/users/2.jpg">
                            </div>
                            <div class="sl-right">
                                <div class="font-medium">Go to the Doctor <span class="sl-date">5 minutes ago</span></div>
                                <div class="desc">Contrary to popular belief</div>
                            </div>
                        </div>
                        <div class="sl-item">
                            <div class="sl-left"> <img class="rounded-circle" alt="user" src="/mendy-admin/assets/images/users/1.jpg">
                            </div>
                            <div class="sl-right">
                                <div><a href="javascript:void(0)">Stephen</a> <span class="sl-date">5 minutes ago</span></div>
                                <div class="desc">Approve meeting with tiger</div>
                            </div>
                        </div>
                        <div class="sl-item">
                            <div class="sl-left bg-primary"> <i class="ti-user"></i></div>
                            <div class="sl-right">
                                <div class="font-medium">Meeting today <span class="sl-date"> 5pm</span></div>
                                <div class="desc">you can write anything </div>
                            </div>
                        </div>
                        <div class="sl-item">
                            <div class="sl-left bg-info"><i class="fas fa-image"></i></div>
                            <div class="sl-right">
                                <div class="font-medium">Send documents to Clark</div>
                                <div class="desc">Lorem Ipsum is simply </div>
                            </div>
                        </div>
                        <div class="sl-item">
                            <div class="sl-left"> <img class="rounded-circle" alt="user" src="/mendy-admin/assets/images/users/4.jpg">
                            </div>
                            <div class="sl-right">
                                <div class="font-medium">Go to the Doctor <span class="sl-date">5 minutes ago</span></div>
                                <div class="desc">Contrary to popular belief</div>
                            </div>
                        </div>
                        <div class="sl-item">
                            <div class="sl-left"> <img class="rounded-circle" alt="user" src="/mendy-admin/assets/images/users/6.jpg">
                            </div>
                            <div class="sl-right">
                                <div><a href="javascript:void(0)">Stephen</a> <span class="sl-date">5 minutes ago</span></div>
                                <div class="desc">Approve meeting with tiger</div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Tab 3 -->
            </div>
        </div>
    </aside>
        <form id="logoutfrm" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>		
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="{{asset('/mendy-admin/assets/libs/jquery/dist/jquery.min.js')}}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{asset('/mendy-admin/assets/libs/popper.js/dist/umd/popper.min.js')}}"></script>
    <script src="{{asset('/mendy-admin/assets/libs/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <!-- apps -->
    <script src="{{asset('/mendy-admin/dist/js/app.min.js')}}"></script>
    <script src="{{asset('/mendy-admin/dist/js/app.init.js')}}"></script>
    <script src="{{asset('/mendy-admin/dist/js/app-style-switcher.js')}}"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{asset('/mendy-admin/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js')}}"></script>
    <script src="{{asset('/mendy-admin/assets/extra-libs/sparkline/sparkline.js')}}"></script>
    <!--Wave Effects -->
    <script src="{{asset('/mendy-admin/dist/js/waves.js')}}"></script>
    <!--Menu sidebar -->
    <script src="{{asset('/mendy-admin/dist/js/sidebarmenu.js')}}"></script>
    <script src="{{asset('/mendy-admin/dist/js/feather.min.js')}}"></script>
    <!--Custom JavaScript -->
    <script src="{{asset('/mendy-admin/dist/js/custom.min.js')}}"></script>
    <!--This page JavaScript -->
    <script src="{{asset('/mendy-admin/assets/extra-libs/jvector/jquery-jvectormap-2.0.2.min.js')}}"></script>
    <script src="{{asset('/mendy-admin/assets/extra-libs/jvector/jquery-jvectormap-us-aea-en.js')}}"></script>
    <!--chartist chart-->
    <script src="{{asset('/mendy-admin/assets/libs/chartist/dist/chartist.min.js')}}"></script>
    <script src="{{asset('/mendy-admin/assets/libs/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.min.js')}}"></script>
    <script src="{{asset('/mendy-admin/assets/libs/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{asset('/mendy-admin/dist/js/pages/dashboards/dashboard4.js')}}"></script>
    <script>
        $('.datepicker').datepicker({
            autoclose: true,
            todayHighlight: true
        });
    </script>
	
    <!--This page plugins -->
    <script src="{{asset('/mendy-admin/assets/extra-libs/DataTables/datatables.min.js')}}"></script>
    <script src="{{asset('/mendy-admin/cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('/mendy-admin/cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js')}}"></script>
    <script src="{{asset('/mendy-admin/cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js')}}"></script>
    <script src="{{asset('/mendy-admin/cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js')}}"></script>
    <script src="{{asset('/mendy-admin/cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js')}}"></script>
    <script src="{{asset('/mendy-admin/cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('/mendy-admin/cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('/mendy-admin/dist/js/pages/datatable/datatable-basic.init.js')}}"></script>	
	
	
	
                
<script>
window.onload = function () {

var chart = new CanvasJS.Chart("chartContainerA", {
	animationEnabled: true,
	//theme: "dark2",//"dark2",
	backgroundColor: "#30CCFF",//"#C2B09A",
	color:"#FA5DDB",
	title:{
		text: "Last 7 Days Data of Total Certification",
		fontColor:"#000",//"#FFF5D9",
		fontFamily: "Roboto,sans-serif",
	},	
	axisX: {
		//title: "Number of Certificate",
		titleFontColor: "#FFF",
		lineColor: "#FFF",
		labelFontColor: "#FFF",
		tickColor: "#FFF"
	},
	axisY: {
		title: "Number of Certificate",
		titleFontColor: "#FFF",
		lineColor: "#FFF",
		labelFontColor: "#FFF",
		tickColor: "#FFF"
	},
	axisY2: {
		title: "",
		titleFontColor: "#FFF",
		lineColor: "#FFF",
		labelFontColor: "#FFF",
		tickColor: "#FFF"
	},	
	toolTip: {
		shared: true
	},
	legend: {
		cursor:"pointer",
		itemclick: toggleDataSeries
	},
	data: [{
		type: "column",
		name: "Total Submitted Certificate",
		legendText: "Total Submitted Certificate",
		showInLegend: true, 
		
		color: "#E7B64F",
		dataPoints:[
			{ label: "24 Jun", y: 266 },
			{ label: "25 Jun", y: 302 },
			{ label: "27 Jun", y: 157 },
			{ label: "28 Jun", y: 148 },
			{ label: "29 Jun", y: 101 },
			{ label: "30 Jun", y: 97 }
		]
	},
	{
		type: "column",	
		name: "Submitted Certificate",
		legendText: "Submitted Certificate",
		//axisYType: "secondary",
		showInLegend: true,
		color: "#F5D4A6",
		dataPoints:[
			{ label: "24 Jun", y: 60 },
			{ label: "25 Jun", y: 80 },
			{ label: "27 Jun", y: 40 },
			{ label: "28 Jun", y: 48 },
			{ label: "29 Jun", y: 29 },
			{ label: "30 Jun", y: 45 }
		]
	}]
});
chart.render();

function toggleDataSeries(e) {
	if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
		e.dataSeries.visible = false;
	}
	else {
		e.dataSeries.visible = true;
	}
	chart.render();
}



var chart = new CanvasJS.Chart("chartContainer", {
	animationEnabled: true,
	//theme: "dark2",//"dark2",
	backgroundColor: "#ECC27D",
	color:'#000',
	title:{
		text: "Last 7 Days Data of Pending Certification",
		color:'#000',
		fontColor:"#000",
		fontFamily: "Roboto,sans-serif",		
	},
	axisX:{
		valueFormatString: "DD MMM",
		titleFontColor: "#008F44",
		lineColor: "#008F44",
		labelFontColor: "#008F44",
		tickColor: "#008F44",		
		crosshair: {
			enabled: true,
			snapToDataPoint: true
		}
	},
	axisY: {
		title: "Number of Certificate",
		titleFontColor: "#008F44",
		lineColor: "#008F44",
		labelFontColor: "#008F44",
		tickColor: "#008F44",		
		crosshair: {
			enabled: true
		}
	},
	toolTip:{
		shared:true
	},  
	legend:{
		cursor:"pointer",
		verticalAlign: "bottom",
		horizontalAlign: "left",
		dockInsidePlotArea: true,
		itemclick: toogleDataSeries
	},
	data: [{
		type: "line",
		showInLegend: true,
		name: "Total Pending",
		markerType: "square",
		xValueFormatString: "DD MMM, YYYY",
		color: "#008F44",
		dataPoints: [
			{ x: new Date(2020, 0, 1), y: 650 },
			{ x: new Date(2020, 0, 3), y: 650 },
			{ x: new Date(2020, 0, 4), y: 700 },
			{ x: new Date(2020, 0, 5), y: 710 },
			{ x: new Date(2020, 0, 6), y: 658 },
			
			{ x: new Date(2020, 0, 8), y: 963 },
			{ x: new Date(2020, 0, 9), y: 847 },
			{ x: new Date(2020, 0, 10), y: 853 },
			{ x: new Date(2020, 0, 11), y: 869 },
			{ x: new Date(2020, 0, 12), y: 943 },
			{ x: new Date(2020, 0, 13), y: 970 },
			{ x: new Date(2020, 0, 14), y: 869 },
			{ x: new Date(2020, 0, 15), y: 890 },
			{ x: new Date(2020, 0, 16), y: 930 }
		]
	},
	{
		type: "line",
		showInLegend: true,
		name: "Your Pending",
		//lineDashType: "dash",
		color: "#F89C0F",
		dataPoints: [
			{ x: new Date(2020, 0, 1), y: 510 },
			{ x: new Date(2020, 0, 3), y: 510 },
			{ x: new Date(2020, 0, 4), y: 560 },
			{ x: new Date(2020, 0, 5), y: 540 },
			{ x: new Date(2020, 0, 6), y: 558 },
			
			{ x: new Date(2020, 0, 8), y: 693 },
			{ x: new Date(2020, 0, 9), y: 657 },
			{ x: new Date(2020, 0, 10), y: 663 },
			{ x: new Date(2020, 0, 11), y: 639 },
			{ x: new Date(2020, 0, 12), y: 673 },
			{ x: new Date(2020, 0, 13), y: 660 },
			{ x: new Date(2020, 0, 14), y: 562 },
			{ x: new Date(2020, 0, 15), y: 643 },
			{ x: new Date(2020, 0, 16), y: 570 }
		]
	}]
});
chart.render();

function toogleDataSeries(e){
	if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
		e.dataSeries.visible = false;
	} else{
		e.dataSeries.visible = true;
	}
	chart.render();
}



var chart = new CanvasJS.Chart("chartContainerB", {
	exportEnabled: true,
	animationEnabled: true,
	backgroundColor: "#FFBA98",
	title:{
		text: "Last 7 Days Data of Reviewed Certification",
		fontColor:"#000",
		fontFamily: "Roboto,sans-serif",		
	},
	legend:{
		cursor: "pointer",
		itemclick: explodePie
	},
	data: [{
		type: "pie",
		showInLegend: true,
		toolTipContent: "{name}: <strong>{y}%</strong>",
		indexLabel: "{name} - {y}%",
		dataPoints: [
			{ y: 26, name: "Agent 1", exploded: true },
			{ y: 20, name: "Agent 2" },
			{ y: 5, name: "Agent 3" },
			{ y: 3, name: "Agent 4" },
			{ y: 7, name: "Agent 5" },
			{ y: 17, name: "Agent 6" },
			{ y: 22, name: "Agent 7"}
		]
	}]
});
chart.render();


var chart = new CanvasJS.Chart("chartContainerC", {
	animationEnabled: true,
	backgroundColor: "#BC8E41",
	title:{
		text: "Last 7 Days Data of Approved Certification",
		fontColor:"#000",
		fontFamily: "Roboto,sans-serif",		
	},
	axisY: {
		title: "Medals"
	},
	legend: {
		cursor:"pointer",
		itemclick : toggleDataSeries
	},
	toolTip: {
		shared: true,
		content: toolTipFormatter
	},
	data: [{
		type: "bar",
		showInLegend: true,
		name: "Gold",
		color: "gold",
		dataPoints: [
			{ y: 243, label: "Agent 1" },
			{ y: 236, label: "Agent 2" },
			{ y: 243, label: "Agent 3" },
			{ y: 273, label: "Agent 4" },
			{ y: 269, label: "Agent 5" },
			{ y: 196, label: "Agent 6" },
			{ y: 1118, label: "Agent 7" }
		]
	},
	{
		type: "bar",
		showInLegend: true,
		name: "Silver",
		color: "silver",
		dataPoints: [
			{ y: 212, label: "Agent 1" },
			{ y: 186, label: "Agent 2" },
			{ y: 272, label: "Agent 3" },
			{ y: 299, label: "Agent 4" },
			{ y: 270, label: "Agent 5" },
			{ y: 165, label: "Agent 6" },
			{ y: 896, label: "Agent 7" }
		]
	},
	{
		type: "bar",
		showInLegend: true,
		name: "Bronze",
		color: "#A57164",
		dataPoints: [
			{ y: 236, label: "Agent 1" },
			{ y: 172, label: "Agent 2" },
			{ y: 309, label: "Agent 3" },
			{ y: 302, label: "Agent 4" },
			{ y: 285, label: "Agent 5" },
			{ y: 188, label: "Agent 6" },
			{ y: 788, label: "Agent 7" }
		]
	}]
});
chart.render();

function toolTipFormatter(e) {
	var str = "";
	var total = 0 ;
	var str3;
	var str2 ;
	for (var i = 0; i < e.entries.length; i++){
		var str1 = "<span style= \"color:"+e.entries[i].dataSeries.color + "\">" + e.entries[i].dataSeries.name + "</span>: <strong>"+  e.entries[i].dataPoint.y + "</strong> <br/>" ;
		total = e.entries[i].dataPoint.y + total;
		str = str.concat(str1);
	}
	str2 = "<strong>" + e.entries[0].dataPoint.label + "</strong> <br/>";
	str3 = "<span style = \"color:Tomato\">Total: </span><strong>" + total + "</strong><br/>";
	return (str2.concat(str)).concat(str3);
}

function toggleDataSeries(e) {
	if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
		e.dataSeries.visible = false;
	}
	else {
		e.dataSeries.visible = true;
	}
	chart.render();
}

}

function explodePie (e) {
	if(typeof (e.dataSeries.dataPoints[e.dataPointIndex].exploded) === "undefined" || !e.dataSeries.dataPoints[e.dataPointIndex].exploded) {
		e.dataSeries.dataPoints[e.dataPointIndex].exploded = true;
	} else {
		e.dataSeries.dataPoints[e.dataPointIndex].exploded = false;
	}
	e.chart.render();

}


</script>
<script src="{{asset('js/canvasjs.min.js')}}"></script>
<script>
lgfrm = function(){
	$('#logoutfrm').submit();
} 
</script>
</body>
</html>