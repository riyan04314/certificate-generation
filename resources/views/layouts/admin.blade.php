<!DOCTYPE html>
<html dir="ltr" lang="en">

<!-- Mirrored from jthemes.org/demo-admin/mendy-admin/html/ltr/index4.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 26 Jun 2020 01:54:01 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('images/sitelogo.jpg')}}">
    <title>{{ trans('panel.site_title') }}</title>
    <!-- Custom CSS -->
    <link href="{{asset('/mendy-admin/assets/extra-libs/jvector/jquery-jvectormap-2.0.2.css')}}" rel="stylesheet" />
    <link href="{{asset('/mendy-admin/assets/libs/chartist/dist/chartist.min.css')}}" rel="stylesheet">
    <link href="{{asset('/mendy-admin/assets/libs/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
    <!-- Custom CSS -->
	<link href="{{asset('/mendy-admin/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css')}}" rel="stylesheet">	



    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" />
    <link href="{{asset('css/font-awesome.css')}}" rel="stylesheet" />
    <link href="{{asset('css/all.css')}}" rel="stylesheet" />
    <link href="{{asset('css/jquery.dataTables.min.css')}}" rel="stylesheet" />
    <link href="{{asset('css/dataTables.bootstrap4.min.css')}}" rel="stylesheet" />
	
    <link href="{{asset('css/buttons.dataTables.min.css')}}" rel="stylesheet" />
    <link href="{{asset('css/select.dataTables.min.css')}}" rel="stylesheet" />
    <link href="{{asset('css/select2.min.css')}}" rel="stylesheet" />
    <link href="{{asset('css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" />
    <link href="{{asset('css/coreui.min.css')}}" rel="stylesheet" />
    <link href="{{asset('css/font-awesome.css')}}" rel="stylesheet" />
    <link href="{{asset('css/dropzone.min.css')}}" rel="stylesheet" />




    <link href="{{asset('/mendy-admin/dist/css/style.min.css')}}" rel="stylesheet">
	<link href="{{asset('/mendy-admin/dist/css/customstyle.css')}}" rel="stylesheet">
<style>
label{
	font-weight:normal;
}
</style>	
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="{{asset('js/html5shiv.js')}}"></script>
    <script src="{{asset('js/respond.min.js')}}"></script>
<![endif]-->
</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <div class="navbar-header">
                    <!-- This is for the sidebar toggle which is visible on mobile only -->
                    <a class="nav-toggler d-block d-md-none" href="javascript:void(0)">
                        <i class="ti-menu ti-close"></i>
                    </a>
                    <!-- ============================================================== -->
                    <!-- Logo -->
                    <!-- ============================================================== -->
                    <div class="navbar-brand">
                        <a href="index.html" class="logo">
                            <!-- Logo icon -->
                            <b class="logo-icon">
                                <?php /*<!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                                <!-- Dark Logo icon -->
                                <img src="{{asset('images/sitelogo.jpg')}}" width="40" alt="homepage" class="dark-logo" />
                                <!-- Light Logo icon -->
                                <img src="{{asset('images/sitelogo.jpg')}}" width="40"  alt="homepage" class="light-logo" />*/ ?>
                            </b>
                            <!--End Logo icon -->
                            <!-- Logo text -->
                            <span class="logo-text">
                                <?php /*<!-- dark Logo text -->
                                <img src="{{asset('images/sitelogo.jpg')}}" alt="homepage" class="dark-logo" />
                                <!-- Light Logo text -->
                                <img src="{{asset('images/sitelogo.jpg')}}" class="light-logo" alt="homepage" />*/ ?>
                            </span>
                        </a>
                        <a class="sidebartoggler d-none d-lg-block" href="javascript:void(0)" data-sidebartype="mini-sidebar">
                            <i data-feather="menu"></i>
                        </a>
                    </div>
                    <!-- ============================================================== -->
                    <!-- End Logo -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- Toggle which is visible on mobile only -->
                    <!-- ============================================================== -->
                    <a class="topbartoggler d-block d-md-none" href="javascript:void(0)" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                        aria-label="Toggle navigation">
                        <i class="ti-more"></i>
                    </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse collapse" id="navbarSupportedContent">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav float-left mr-auto">
                        <li class="nav-item dropdown mega-dropdown">
                            <a class="nav-link dropdown-toggle waves-effect waves-dark" href="#" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                            </a>
                        </li>
                    </ul>
                    <!-- ============================================================== -->
                    <!-- Right side toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav float-right">
                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle waves-effect waves-dark pro-pic" href="#" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                                <span class="mr-3 font-normal d-none d-sm-inline-block pro-name"><span>Hello,</span>
								{{Auth::user()->name}}</span>
                                <img src="{{asset('images/sitelogo.jpg')}}" alt="user" class="rounded-circle" width="40">
                            </a>
                            <div class="dropdown-menu dropdown-menu-right user-dd animated flipInY">
                                <div class="d-flex no-block align-items-center p-3 bg-light mb-2">
                                    <div class="">
                                        <img src="{{asset('images/sitelogo.jpg')}}" alt="user" class="rounded-circle"
                                            width="60">
                                    </div>
                                    <div class="ml-2">                                       
                                        <h4 class="mb-0">{{ Auth::user()->department}}</h4>
                                        <p class="mb-0">{{ Auth::user()->email }}</p>																				
                                    </div>
                                </div>
                                <div class="profile-dis scrollable">
                                    <?php /*<a class="dropdown-item" href="javascript:void(0)">
                                        <i class="ti-user mx-1"></i> My Profile</a>
                                    <a class="dropdown-item" href="javascript:void(0)">
                                        <i class="ti-wallet mx-1"></i> My Balance</a>
                                    <a class="dropdown-item" href="javascript:void(0)">
                                        <i class="ti-email mx-1"></i> Inbox</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="javascript:void(0)">
                                        <i class="ti-settings mx-1"></i> Account Setting</a>*/ ?>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="javascript:void(0)"  onclick="event.preventDefault(); document.getElementById('logoutfrm').submit();">
                                        <i class="fa fa-power-off mx-1"></i> Logout</a>										
                                </div>
                            </div>
                        </li>
                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        @include('partials.menu')
		<!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
			@if(isset($page) && $page == 'home')

			@endif
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid @if(isset($page) && $page == 'home'){{'negative-margin'}}@endif" style="background-color:#b9883b;">
                @if(session('message'))
                    <div class="row mb-2">
                        <div class="col-lg-12">
                            <div class="alert alert-success" role="alert">{{ session('message') }}</div>
                        </div>
                    </div>
                @endif
                @if($errors->count() > 0)
                    <div class="alert alert-danger">
                        <ul class="list-unstyled">
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @yield('content')                
			</div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer text-center">
                All Rights Reserved by {{ trans('panel.site_title') }}
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- customizer Panel -->
    <!-- ============================================================== -->

        <form id="logoutfrm" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>	
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="{{asset('/mendy-admin/assets/libs/jquery/dist/jquery.min.js')}}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{asset('/mendy-admin/assets/libs/popper.js/dist/umd/popper.min.js')}}"></script>
    <script src="{{asset('/mendy-admin/assets/libs/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <!-- apps -->
    <script src="{{asset('/mendy-admin/dist/js/app.min.js')}}"></script>
    <script src="{{asset('/mendy-admin/dist/js/app.init.js')}}"></script>
    <script src="{{asset('/mendy-admin/dist/js/app-style-switcher.js')}}"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{asset('/mendy-admin/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js')}}"></script>
    <script src="{{asset('/mendy-admin/assets/extra-libs/sparkline/sparkline.js')}}"></script>
    <!--Wave Effects -->
    <script src="{{asset('/mendy-admin/dist/js/waves.js')}}"></script>
    <!--Menu sidebar -->
    <script src="{{asset('/mendy-admin/dist/js/sidebarmenu.js')}}"></script>
    <script src="{{asset('/mendy-admin/dist/js/feather.min.js')}}"></script>
    <!--Custom JavaScript -->
    <script src="{{asset('/mendy-admin/dist/js/custom.min.js')}}"></script>
    <!--This page JavaScript -->
    <script src="{{asset('/mendy-admin/assets/extra-libs/jvector/jquery-jvectormap-2.0.2.min.js')}}"></script>
    <script src="{{asset('/mendy-admin/assets/extra-libs/jvector/jquery-jvectormap-us-aea-en.js')}}"></script>
    <!--chartist chart-->
    <script src="{{asset('/mendy-admin/assets/libs/chartist/dist/chartist.min.js')}}"></script>
    <script src="{{asset('/mendy-admin/assets/libs/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.min.js')}}"></script>
    <script src="{{asset('/mendy-admin/assets/libs/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{asset('/mendy-admin/dist/js/pages/dashboards/dashboard4.js')}}"></script>
    <script>
        $('.datepicker').datepicker({
            autoclose: true,
            todayHighlight: true
        });
    </script>
    <!--This page plugins -->
    <script src="{{asset('/mendy-admin/assets/extra-libs/DataTables/datatables.min.js')}}"></script>
    <script src="{{asset('/mendy-admin/cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('/mendy-admin/cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js')}}"></script>
    <?php /*<script src="/mendy-admin/cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="/mendy-admin/cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>*/ ?>
    <?php /*<script src="/mendy-admin/cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>*/ ?>
    <script src="{{asset('/mendy-admin/cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('/mendy-admin/cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('/mendy-admin/dist/js/pages/datatable/datatable-basic.init.js')}}"></script>
	
	<?php if(1){	?>
	
	
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/popper.min.js')}}"></script>
    <script src="{{asset('js/coreui.min.js')}}"></script>
    <script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('js/buttons.flash.min.js')}}"></script>
    <script src="{{asset('js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('js/buttons.print.min.js')}}"></script>
    <script src="{{asset('js/buttons.colVis.min.js')}}"></script>
    <script src="{{asset('js/pdfmake.min.js')}}"></script>
    <script src="{{asset('js/vfs_fonts.js')}}"></script>
    <script src="{{asset('js/jszip.min.js')}}"></script>
    <script src="{{asset('js/dataTables.select.min.js')}}"></script>
    <script src="{{asset('js/ckeditor.js')}}"></script>
    <script src="{{asset('js/moment.min.js')}}"></script>
    <script src="{{asset('js/bootstrap-datetimepicker.min.js')}}"></script>
    <script src="{{asset('js/select2.full.min.js')}}"></script>
    <script src="{{asset('js/dropzone.min.js')}}"></script>
    <script src="{{ asset('js/main.js') }}"></script>

	

	<script type="text/javascript" src="{{ asset('js/daterangepicker.min.js')}}"></script>
	<link rel="stylesheet" type="text/css" href="{{ asset('css/daterangepicker.css')}}" />
	
    <script src="{{ asset('js/main.js') }}"></script>
    <script>
        $(function() {
			$('input[name="courseduration"]').daterangepicker({locale: {format: 'YYYY/MM/DD'}});
		  let copyButtonTrans = '{{ trans('global.datatables.copy') }}'
		  let csvButtonTrans = '{{ trans('global.datatables.csv') }}'
		  let excelButtonTrans = '{{ trans('global.datatables.excel') }}'
		  let pdfButtonTrans = '{{ trans('global.datatables.pdf') }}'
		  let printButtonTrans = '{{ trans('global.datatables.print') }}'
		  let colvisButtonTrans = '{{ trans('global.datatables.colvis') }}'
		  let selectAllButtonTrans = '{{ trans('global.select_all') }}'
		  let selectNoneButtonTrans = '{{ trans('global.deselect_all') }}'

		  let languages = {
			'en': '{{asset("js/i18n/English.json")}}'
		  };

		  $.extend(true, $.fn.dataTable.Buttons.defaults.dom.button, { className: 'btn' })
		  $.extend(true, $.fn.dataTable.defaults, {
			language: {
			  url: languages['{{ app()->getLocale() }}']
			},
			columnDefs: [{
				orderable: false,
				className: 'select-checkbox',
				targets: 0
			}, {
				orderable: false,
				searchable: false,
				targets: -1
			}],
			select: {
			  style:    'multi+shift',
			  selector: 'td:first-child'
			},
			order: [],
			scrollX: true,
			pageLength: 100,
			dom: 'lBfrtip<"actions">',
			buttons: [
			  {
				extend: 'selectAll',
				className: 'btn-primary',
				text: selectAllButtonTrans,
				exportOptions: {
				  columns: ':visible'
				},
				action: function(e, dt) {
				  e.preventDefault()
				  dt.rows().deselect();
				  dt.rows({ search: 'applied' }).select();
				}
			  },
			  {
				extend: 'selectNone',
				className: 'btn-primary',
				text: selectNoneButtonTrans,
				exportOptions: {
				  columns: ':visible'
				}
			  },
			  {
				extend: 'copy',
				className: 'btn-default',
				text: copyButtonTrans,
				exportOptions: {
				  columns: ':visible'
				}
			  },
			  {
				extend: 'csv',
				className: 'btn-default',
				text: csvButtonTrans,
				exportOptions: {
				  columns: ':visible'
				}
			  },
			  {
				extend: 'excel',
				className: 'btn-default',
				text: excelButtonTrans,
				exportOptions: {
				  columns: ':visible'
				}
			  },
			  {
				extend: 'pdf',
				className: 'btn-default',
				text: pdfButtonTrans,
				exportOptions: {
				  columns: ':visible'
				}
			  },
			  {
				extend: 'print',
				className: 'btn-default',
				text: printButtonTrans,
				exportOptions: {
				  columns: ':visible'
				}
			  },
			  {
				extend: 'colvis',
				className: 'btn-default',
				text: colvisButtonTrans,
				exportOptions: {
				  columns: ':visible'
				}
			  }
			]
		  });

		  $.fn.dataTable.ext.classes.sPageButton = '';
		});

    </script>
	
    
	
	<?php	}	?>
	
	
	
<script>
lgfrm = function(){
	
	$('#logoutfrm').submit();
} 

</script>	
@yield('scripts')	
</body>


<!-- Mirrored from jthemes.org/demo-admin/mendy-admin/html/ltr/index4.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 26 Jun 2020 01:56:58 GMT -->
</html>
