<?php

return [
    'password' => 'يجب أن تتكون كلمات المرور من ستة أحرف على الأقل وأن تطابق التأكيد.',
    'reset'    => 'Your password has been reset!',
    'sent'     => 'We have e-mailed your password reset link!',
    'token'    => 'This password reset token is invalid.',
    'user'     => 'We can\'t find a user with that e-mail address.',
    'updated'  => 'Your password has been changed!',
];
