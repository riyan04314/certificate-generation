<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
		return view('home');
    }
	
	public function imagickdev()
    {
		$img = '/images/ulogo.jpg';
		
		if (!extension_loaded('imagick')){
			echo 'imagick not installed';
		}
		
		
		$str = chr(rand(65,90)).chr(rand(65,90)).chr(rand(65,90)).chr(rand(65,90)).chr(rand(65,90)).rand(0,9).substr(time(),5);		
		\QrCode::size(500)->format('png')->generate('Certificate'.$str, public_path('images/qrcode/qrcode-'.$str.'.png'));
		echo '<img src="images/qrcode/qrcode-'.$str.'.png" width="250" style="margin:20px;" /> &nbsp; &nbsp;';
		\QrCode::size(500)->format('png')->generate('Certificate'.$str, public_path('images/qrcode/qrcode-'.$str.'-1.png'));
		echo '<img src="images/qrcode/qrcode-'.$str.'-1.png" width="250" style="margin:20px;" /> &nbsp; &nbsp;';
		
		/*$str = chr(rand(65,90)).chr(rand(65,90)).chr(rand(65,90)).chr(rand(65,90)).chr(rand(65,90)).rand(0,9).substr(time(),5);		
		\QrCode::size(500)->format('png')->generate('Certificate'.$str, public_path('images/qrcode/qrcode-'.$str.'.png'));
		echo '<img src="images/qrcode/qrcode-'.$str.'.png" width="250" style="margin:20px;" />';
		*/
		
		//$imgs = new \Imagick($img);
		dd('imagickdev');
		//return view('home');
    }
	
	
	
}
