<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\CsvImportTrait;
use App\Http\Requests\MassDestroyInternalmemberRequest;
use App\Http\Requests\StoreInternalmemberRequest;
use App\Http\Requests\UpdateInternalmemberRequest;
use App\Internalmember;
use App\Department;
use Gate;
use Auth;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class InternalmemberController extends Controller
{
    use CsvImportTrait;

    public function index()
    {
        abort_if(Gate::denies('internalmember_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
	if(Auth::user()->department == NULL){
		$internalmembers = Internalmember::get();
//		dd($internalmembers);
	}
	else{
		$internalmembers = Internalmember::where('department', Auth::user()->department)->get();
		//$internalmembers = Internalmember::where('emp_type', Auth::user()->user_type)->where('department', Auth::user()->department)->get();			
	}

	//dd(Auth::user()->user_type);
        //dd(Auth::user()->user_type,$internalmembers);
        return view('admin.internalmembers.index', compact('internalmembers'));
    }

    public function create()
    {
        abort_if(Gate::denies('internalmember_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');
		$department = Department::get();
        return view('admin.internalmembers.create',compact('department'));
    }

    public function store(StoreInternalmemberRequest $request)
    {
	$internalmember = Internalmember::create($request->all());
	//$res = Internalmember::get();

	//dd($res->toArray(),$request->all());
        return redirect()->route('admin.internalmembers.index');
    }

    public function edit(Internalmember $internalmember)
    {
        abort_if(Gate::denies('internalmember_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.internalmembers.edit', compact('internalmember'));
    }

    public function update(UpdateInternalmemberRequest $request, Internalmember $internalmember)
    {
        $internalmember->update($request->all());

        return redirect()->route('admin.internalmembers.index');
    }

    public function show(Internalmember $internalmember)
    {
        abort_if(Gate::denies('internalmember_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.internalmembers.show', compact('internalmember'));
    }

    public function destroy(Internalmember $internalmember)
    {
        abort_if(Gate::denies('internalmember_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $internalmember->delete();

        return back();
    }

    public function massDestroy(MassDestroyInternalmemberRequest $request)
    {
        Internalmember::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
