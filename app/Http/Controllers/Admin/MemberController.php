<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\CsvImportTrait;
use App\Http\Requests\MassDestroyMemberRequest;
use App\Http\Requests\StoreMemberRequest;
use App\Http\Requests\UpdateMemberRequest;
use App\Certificate;
use App\Employee;
use App\User;
use App\Department;
use Gate;
use PDF;
use Auth;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;


class MemberController extends Controller
{
    use CsvImportTrait;

    public function index()
    {
        $employees = Employee::all();		
        abort_if(Gate::denies('employee_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
		
		/*
		$type = 'external';
		if($request->type == 'internal'){
			$type = 'internal';
		}
		*/
		
		$certificate = array();
		$res = Certificate::select('id','certificate_title','department')->get()->toArray();
		
		foreach($res as $val){
			$certificate[$val['id']]['id'] = $val['id'];
			$certificate[$val['id']]['certificate_title'] 	= $val['certificate_title'];
			$certificate[$val['id']]['department'] 			= $val['department'];
		}
		//dd($certificate);
        $employees = Employee::get();
		
		if(Auth::user()->roles[0]->title == 'Admin' || Auth::user()->roles[0]->title == 'Principal'){
			$employees = Employee::where('employee_type',1)->get();
		}
		elseif(Auth::user()->roles[0]->title == 'Agent'){
			$employees = Employee::where('employee_type',1)->where('created_by',Auth::user()->id)->get();
		}
		else{
			
			$employees = Employee::where('employee_type',1)->where('emp_type',Auth::user()->user_type)->where('department',Auth::user()->department)->get();
			//dd($employees);
		}
		
		
		
		
        $users = User::all();
		$usersname = array();
		foreach($users as $val){
			$usersname[$val->id] = $val->name;
		}		
		
		
		$type = 1;
		$mass_actiontxt = '';
		if(Auth::user()->roles[0]->title == 'Manager'){
			$mass_actiontxt = 'Reviewed';
		}
		elseif(Auth::user()->roles[0]->title == 'HOD'){
			$mass_actiontxt = 'Approved';
		}		
        return view('admin.employees.external.index', compact('employees','certificate','usersname','type','mass_actiontxt'));
    }

    public function create()
    {
        //abort_if(Gate::denies('member_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');
		$certificate = array();
		//dd(Auth::user()->roles[0]->title);
		if(Auth::user()->roles[0]->title == 'Admin' || Auth::user()->roles[0]->title === 'Principal'){
			$res = Certificate::select('id','certificate_title','department','serial_prefix','serial_no')->get()->toArray();
		}
		else{
			$res = Certificate::select('id','certificate_title','department','serial_prefix','serial_no')->where('department',Auth::user()->department)->get()->toArray();
		}

		
		foreach($res as $val){
			$certificate[$val['id']]['id'] = $val['id'];
			$certificate[$val['id']]['certificate_title'] = $val['certificate_title'];
			$certificate[$val['id']]['department'] 			= $val['department'];
			$certificate[$val['id']]['serial_prefix'] 		= $val['serial_prefix'];
			$certificate[$val['id']]['serial_no'] 			= $val['serial_no']+1;
			$serial_no 										= $val['serial_no']+1;				
		}
		$type = 1;
	$department = Department::get();		
        return view('admin.employees.external.create', compact('certificate','type','department','serial_no'));
    }

    public function store(StoreMemberRequest $request)
    {
//	dd('');
        $member = Member::create($request->all());

        return redirect()->route('admin.members.index');
    }

    public function edit(Member $member)
    {
        abort_if(Gate::denies('member_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.employees.internal.edit', compact('member'));
    }

    public function update(UpdateMemberRequest $request, Member $member)
    {
        $member->update($request->all());

        return redirect()->route('admin.members.index');
    }

    public function show(Member $member)
    {
        abort_if(Gate::denies('member_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.members.show', compact('member'));
    }

    public function destroy(Member $member)
    {
        abort_if(Gate::denies('member_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $member->delete();

        return back();
    }

    public function massDestroy(MassDestroyMemberRequest $request)
    {
		dd(9339);
        Member::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
