<?php

namespace App\Http\Requests;

use App\Internalmember;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyInternalmemberRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('internalmember_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:internalmembers,id',
        ];
    }
}
