<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \DateTimeInterface;

class Employee extends Model
{
    use SoftDeletes;

    public $table = 'employees';

    protected $dates = [
        'certificate_approve_date',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    const EMPLOYEE_TYPE_SELECT = [
        '1' => 'Internal Employee',
        '2' => 'External Employee',
    ];

    const CERTIFICATE_APPROVAL_STATUS_SELECT = [
        '1' => 'Pending',
        '2' => 'Approved',
        '3' => 'Canceled',
		'4' => 'Reviewed',
    ];

    const EMP_CATEGORY_SELECT = [
        '1' => 'السيد.',
        '2' => 'السيدة.',
        '3' => 'تصلب متعدد..',
        '4' => 'دكتور.',
        '5' => 'مهندس',
		'6' => 'الآخرين',		
		'7' => 'تناتيتسن',
        '8' => 'ملازم أول'
    ];

    protected $fillable = [
		'emp_type',
		'emp_id',
		'emailid',
        'emp_category',
        'first_name',
        'last_name',
		'courseduration',
		'department',
        'institution_name',
        'issue_date',
        'serial_no',		
        'certificate_approval_status',
        'certificate_approve_by',
        'certificate_qrcode',
        'certificate_approve_date',
        'default_certificate',
        'employee_type',
        'created_by',
		'reviewed_by',
		'pdf_generated',
		'pdf_name_setup',
		'mail_sent',
		'mail_sent_schedule',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function getCertificateApproveDateAttribute($value)
    {
        return $value ? Carbon::parse($value)->format(config('panel.date_format')) : null;
    }

    public function setCertificateApproveDateAttribute($value)
    {
        $this->attributes['certificate_approve_date'] = $value ? Carbon::createFromFormat(config('panel.date_format'), $value)->format('Y-m-d') : null;
    }
}
