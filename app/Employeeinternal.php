<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \DateTimeInterface;

class Employeeinternal extends Model
{
    use SoftDeletes;

    public $table = 'employees';

    protected $dates = [
        'certificate_approve_date',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    const EMPLOYEE_TYPE_SELECT = [
        '1' => 'Internal Employee',
        '2' => 'External Employee',
    ];

    const CERTIFICATE_APPROVAL_STATUS_SELECT = [
        '1' => 'Pending',
        '2' => 'Approved',
        '3' => 'Canceled',
		'4' => 'Reviewed',
    ];

    const EMP_CATEGORY_SELECT = [
        '1' => 'Mr.',
        '2' => 'Mrs.',
        '3' => 'Ms.',
        '4' => 'Dr.',
        '5' => 'Eng.',
        '6' => 'Others',
    ];

    protected $fillable = [
		'emp_id',
		'emailid',
        'emp_category',
        'first_name',
        'last_name',
		'department',
        'institution_name',
        'default_certificate',
        'employee_type'
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function getCertificateApproveDateAttribute($value)
    {
        return $value ? Carbon::parse($value)->format(config('panel.date_format')) : null;
    }

    public function setCertificateApproveDateAttribute($value)
    {
        $this->attributes['certificate_approve_date'] = $value ? Carbon::createFromFormat(config('panel.date_format'), $value)->format('Y-m-d') : null;
    }
}
