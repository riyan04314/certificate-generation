<?php
namespace App\Mail;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Certificate;
use App\Employee;
use App\User;
use PDF;
use Illuminate\Support\Facades\Storage;

class MailtrapExample extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
	 
    public function builderr()
    {
        $mail = $this->from('to-email@example.com')
                    ->view('mails.exmpl');

        if(!empty($this->data["attachments"])){
            foreach($this->data["attachments"] as $k => $v){
                $mail = $mail->attach($v["path"], [
                    'as' => $v["as"],
                    'mime' => $v["mime"],
                ]);
            }
        }
                
    }	 

    public function build()
    {
		$arr = $this->data;
		//$arr = $arr['attachments'];
		$file = $arr['attachments'][0]['path'];
		//$file = 'pdf/certificate2.pdf';
		//dd($file,$arr['to']);
		
		
		//dd(Storage::disk( 'public' )->path( 'certificate.pdf' ));
        return $this->from('prasenjit.aluni@gmail.com', 'Mailtrap')
                    ->view( 'mails.exmpl' )
                    ->subject( 'Certificate' )
                    ->attach( $file , [
                        'as'   => 'certificate.pdf',
                        'mime' => 'application/pdf',
                    ] );		
		
		
        return $this->view('mails.exmpl')
                    ->attach(public_path('pdf/certificate.pdf'), [
                         'as' => 'sample.pdf',
                         'mime' => 'application/pdf',
                    ]);		
		
		
		$pdf = PDF::loadView('mails.exmpl');
		return $this->from('prasenjit.aluni@gmail.com', 'Mailtrap')
            ->subject('Certificate')
			->attachData(base64_encode('certificate.pdf')->output(), 'filename1.pdf', [ 'mime'=>'application/jpeg'])
			->attachData($pdf->output(), 'filename.pdf')
			->attach(public_path('images/laravel.jpg'), ['as' => 'Logomimage.jpg', 'mime'=>'application/jpeg'])
			->markdown('mails.exmpl')
			->with([
                'name' => 'New Mailtrap User',
                'link' => 'https://mailtrap.io/inboxes'
            ]);	
	}


}
