<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInternalmembersTable extends Migration
{
    public function up()
    {
        Schema::create('internalmembers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('emp_category');
            $table->string('first_name');
            $table->string('last_name')->nullable();
            $table->string('institution_name')->nullable();
            $table->string('emailid')->nullable();
            $table->string('contact_no')->nullable();
            $table->string('certificate_approval_status')->nullable();
            $table->integer('certificate_approve_by')->nullable();
            $table->date('certificate_approve_date')->nullable();
            $table->longText('certificate_qrcode')->nullable();
            $table->integer('default_certificate')->nullable();
            $table->integer('created_by')->nullable();
            $table->string('employee_type')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
