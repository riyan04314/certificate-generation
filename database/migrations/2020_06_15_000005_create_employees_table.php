<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name')->nullable();
            $table->integer('created_by')->nullable();
            $table->string('institution_name')->nullable();
            $table->integer('default_certificate')->nullable();
            $table->integer('certificate_approve_by');
            $table->longText('certificate_qrcode')->nullable();
            $table->date('certificate_approve_date')->nullable();
            $table->string('employee_type')->nullable();
            $table->string('certificate_approval_status')->nullable();
            $table->string('emp_category');
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
